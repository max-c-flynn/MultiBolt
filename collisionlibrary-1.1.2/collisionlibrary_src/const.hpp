// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "collisionlibrary"


// Numerical constants

static const double HARTREE = 27.211386;		// Hartree energy (eV)

static const int ANGULAR_N = 500; // grid resolution between 0,pi for solid angle integrals (slow-eval)

static const double PI	= 3.14159265358979323846;
static const double KB	= 1.38064852e-23;		// Boltzmann constant (J/K)
static const double QE	= 1.60217663e-19;		// Elementary charge (C)
static const double ME	= 9.10938370e-31;		// Mass of the electron (kg)
static const double AMU = 1.66053906e-27;		// Atomic mass unit (AMU, in kg)

static const double RYDBERG = 13.6;				// Rydberg constant (eV)

static const double A0 = 5.291e-11;				// Bohr radius [m^2]

static const double PLANCK_EV = 6.5821e-16;		// Planck energy in [eV]