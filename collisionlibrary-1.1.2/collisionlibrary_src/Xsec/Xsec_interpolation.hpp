// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_XSECINTERPOLATION
#define COLLISIONLIBRARY_XSECINTERPOLATION
#include "collisionlibrary"

// Logarithmic interpolation only
void crosssection_interp1_logarithmic(const arma::colvec& data_eV, const arma::colvec& data_s, const arma::colvec& point_eV, arma::colvec& point_s) {

	arma::uvec ix = arma::regspace<arma::uvec>(0, 1, data_eV.size() - 1);

	// edge-case: if the earliest data_eV point is eV=0, full log-interp on all data will not work 
	if (arma::any(data_eV == 0)) {
		ix = arma::regspace<arma::uvec>(1, 1, data_eV.size() - 1);
	}

	arma::colvec log10_point_s;
	arma::interp1(log10(data_eV(ix)), log10(data_s(ix)), log10(point_eV), log10_point_s, "linear");

	point_s = arma::exp10(log10_point_s);

	// assume the points which escaped log-interp (nan at this point) need linear interpolation instead
	// this handles the edge case of when a xsec has tabulated data placed at 0 eV
	ix = arma::find_nonfinite(point_s(arma::find(point_eV < max(data_eV))));
	if (ix.size() > 0) {
		arma::colvec lin_point_s;
		arma::interp1(data_eV, data_s, point_eV(ix), lin_point_s, "linear");

		point_s(ix) = lin_point_s;
	}

	return;
}

// Linear interpolation only
void crosssection_interp1_linear(const arma::colvec& data_eV, const arma::colvec& data_s, const arma::colvec& point_eV, arma::colvec& point_s) {

	arma::colvec lin_point_s;
	arma::interp1(data_eV, data_s, point_eV, lin_point_s, "linear");
	point_s = lin_point_s;
	return;
}

// Form to allow choice between log or lin interp
void crosssection_interp1(const arma::colvec& data_eV, const arma::colvec& data_s, const arma::colvec& point_eV, arma::colvec& point_s, 
	const lib::InterpolationMethod interp_method = lib::DEFAULT_INTERP_METHOD) {

	if (interp_method == lib::InterpolationMethod::Linear) {
		crosssection_interp1_linear(data_eV, data_s, point_eV, point_s);
	}
	else if (interp_method == lib::InterpolationMethod::Logarithmic) {
		crosssection_interp1_logarithmic(data_eV, data_s, point_eV, point_s);
	}

	return;
}



// gridded Xsec below any threshold energy should be zero
void enforce_below_thresh_is_zero(const arma::colvec& data_eV, const arma::colvec& data_s, const arma::colvec& eV, arma::colvec& s, const double eV_thresh) {

	arma::uvec below_0_idx = arma::find(eV <= eV_thresh);
	s(below_0_idx).zeros();
	return;
}


// For points in energy beyond what is provided by
// the data, use proportional born-type extrapolation
// requires that s is already non-zero
void enforce_born_extrapolation(const arma::colvec& data_eV, const arma::colvec& data_s, const arma::colvec& eV, arma::colvec& s) {

	// points of s for eV beyond far_point use born extrapolation
	arma::uvec far_idx = arma::find(eV > arma::max(data_eV));

	
	if (far_idx.n_elem == 0) {
		return;
	}


	auto this_row = data_eV.index_max();
	arma::colvec uma = { data_eV(this_row) };
	arma::colvec grid_uma = { data_s(this_row) };


	// final value in the data. Sometimes, this is written as zero, which may screw up the born extrapolation
	if (data_s(data_s.n_elem - 1) == 0) {
		// extrapolate all as zero instead. 
		s.elem(far_idx).zeros();

	}
	else { // continue with the usual born-extrapolation

		arma::colvec born_x = eV;
		arma::colvec born_y = arma::log(born_x) / born_x;

		arma::colvec NF = (arma::log(born_x) / (born_x));
		NF = NF / (log(uma(0)) / uma(0));
		s.elem(far_idx) = NF(far_idx) * grid_uma(0);
	}

	return;
}

// Sometimes you might prefer 1/eV extrapolation
void enforce_inveV_extrapolation(const arma::colvec& data_eV, const arma::colvec& data_s, const arma::colvec& eV, arma::colvec& s) {

	// points of s for eV beyond far_point use extrapolation
	arma::uvec far_idx = arma::find(eV > arma::max(data_eV));

	if (far_idx.n_elem == 0) {
		return;
	}

	auto this_row = data_eV.index_max();
	arma::colvec uma = { data_eV(this_row) };
	arma::colvec grid_uma = { data_s(this_row) };

	// final value in the data. Sometimes, this is written as zero, which may screw up extrapolation
	if (data_s(data_s.n_elem - 1) == 0) {
		// extrapolate all as zero instead. 
		s.elem(far_idx).zeros();

	}
	else { // continue with the usual extrapolation

		arma::colvec born_x = eV;
		arma::colvec born_y = 1.0 / born_x;

		arma::colvec NF = (1.0 / (born_x));
		NF = NF / (log(uma(0)) / uma(0));
		s.elem(far_idx) = NF(far_idx) * grid_uma(0);
	}

	return;
}

// Sometimes you might prefer 1/e^2 extrapolation
void enforce_inveV2_extrapolation(const arma::colvec& data_eV, const arma::colvec& data_s, const arma::colvec& eV, arma::colvec& s) {

	// points of s for eV beyond far_point use extrapolation
	arma::uvec far_idx = arma::find(eV > arma::max(data_eV));

	if (far_idx.n_elem == 0) {
		return;
	}

	auto this_row = data_eV.index_max();
	arma::colvec uma = { data_eV(this_row) };
	arma::colvec grid_uma = { data_s(this_row) };

	// final value in the data. Sometimes, this is written as zero, which may screw up extrapolation
	if (data_s(data_s.n_elem - 1) == 0) {
		// extrapolate all as zero instead. 
		s.elem(far_idx).zeros();

	}
	else { // continue with the usual extrapolation

		arma::colvec born_x = eV;
		arma::colvec born_y = 1.0 / born_x / born_x;

		arma::colvec NF = (1.0 / (born_x) / born_x);
		NF = NF / (log(uma(0)) / uma(0));
		s.elem(far_idx) = NF(far_idx) * grid_uma(0);
	}

	return;
}


// in the rare occasion a particular grid asks for points
// below the data, armadillo may fill these as nan or zero
// both are nonphysical, and should be replaced assuming
// the Xsec is constant
// requires that s is already non-zero
void fill_small(const arma::colvec& data_eV, const arma::colvec& data_s, const arma::colvec& eV, arma::colvec& s) {

	// points of s for eV beyond far_point use born extrapolation
	arma::uvec early_idx = arma::find(eV < data_eV(0));

	if (early_idx.n_elem == 0) {
		return;
	}

	double early_s = data_s(0);
	s(early_idx) = arma::colvec(early_idx.n_elem, arma::fill::ones) * early_s;

	return;
}




void grid_s_as_continuous(const arma::colvec& data_eV, const arma::colvec& data_s, const arma::colvec& eV, arma::colvec& s,
	const lib::InterpolationMethod interp_method = lib::DEFAULT_INTERP_METHOD) {

	lib::crosssection_interp1(data_eV, data_s, eV, s, interp_method);

	lib::enforce_born_extrapolation(data_eV, data_s, eV, s);
	lib::fill_small(data_eV, data_s, eV, s);

	return;
}

void grid_s_as_thresholded(const arma::colvec& data_eV, const arma::colvec& data_s, const arma::colvec& eV, arma::colvec& s, const double eV_thresh,
	const lib::InterpolationMethod interp_method = lib::DEFAULT_INTERP_METHOD) {

	lib::crosssection_interp1(data_eV, data_s, eV, s, interp_method);

	lib::enforce_born_extrapolation(data_eV, data_s, eV, s);
	lib::enforce_below_thresh_is_zero(data_eV, data_s, eV, s, eV_thresh);

	s.replace(arma::datum::nan, 0); // last ditch effort, nans sometimes appear close to threshold
}




// Special "continuous" case for superelastic
// realizes the Klein-Rosseland relation
arma::colvec interp_sup_from_exc(const arma::colvec& data_eV, const arma::colvec& data_s, const double eV_thresh, const arma::colvec& eV, const double g,
	const lib::InterpolationMethod interp_method = lib::DEFAULT_INTERP_METHOD) {

	arma::colvec val(eV.n_elem, arma::fill::zeros);

	lib::crosssection_interp1(data_eV, data_s, eV + eV_thresh, val, interp_method);

	val = (1.0 / g) * val % (1.0 + eV_thresh / eV);


	// occasionally, far points screw up and become nan
	val.replace(arma::datum::nan, 0);
	// take all the zeros this would have just created and turn them into born extrapolation
	arma::uvec idx = arma::find(val != 0);
	
	if (idx.n_elem > 0) {
		lib::enforce_born_extrapolation(eV(idx), val(idx), eV, val);
	}

	// the first point, if at zero, might evaluate as inf. To avoid problems while integrating,
	// it is instead equal to the next value in s


	if (eV(0) == 0) {
		val(0) = val(1);
	}

	return val;
}





#endif

