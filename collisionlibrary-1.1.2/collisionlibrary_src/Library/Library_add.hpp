// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_LIBRARYADD
#define COLLISIONLIBRARY_LIBRARYADD
#include "collisionlibrary"


void lib::Library::add_species_from_LXCat_file(const std::string& Xsec_fid) {


	// first, identify the species that exist in the file
	auto found_pairs = lib::identify_species_in_LXCat_file(Xsec_fid);

	// and the ones that you already have on hand
	std::vector<std::string> existing_species = {};
	for (auto spec : allspecies) {
		existing_species.push_back(spec->_name);
	}


	// add new species libraries to this object from the map above (if they don't exist already).
	for (auto it = found_pairs.begin(); it != found_pairs.end(); ++it) {

		auto name = *it;
		

		// if is not already in vector
		if (std::count(existing_species.begin(), existing_species.end(), name) == 0) {

			add_blank_species(name);

		}
	}

}



void lib::Library::add_xsecs_from_LXCat_files(const std::string& Xsec_fid) {

	add_species_from_LXCat_file(Xsec_fid);

	auto xsecs = lib::make_Xsecs_from_LXCat_file(Xsec_fid);


	for (auto spec : allspecies) {
		for (auto x : xsecs) {

			if (lib::same_string(x->reactant(), spec->_name)) {

				spec->add_Xsec(x);
			}
		}
	}

	return;

}

void lib::Library::add_xsecs_from_LXCat_files(const std::vector<std::string>& Xsec_fids) {
	for (auto& str : Xsec_fids) {
		add_xsecs_from_LXCat_files(str);
	}
}


void lib::Library::add_blank_species(const std::string& name) {

	// does the species already exist?
	if (!has_species(name)) {
		allspecies.push_back(std::make_shared<lib::Species>(lib::Species(name)));
	}
	else {

		lib::normal_statement("Warning: The species with name [" + name + "] was not added because it already exists in the library.");
	}

}





void lib::Library::add_blank_species(const std::vector<std::string>& names) {

	for (auto str : names) {

		add_blank_species(str);

	}


}


void lib::Library::add_Xsec(std::shared_ptr<lib::AbstractXsec> x) {


	// do not add Xsec if it is not fully defined!
	if (x->code() == lib::CollisionCode::nocollision) {
		lib::normal_statement("The Xsec with process [" + x->process() + "] was not added to the library because it was defined as 'nocollision'.");
		return;
	}

	// fixed, actually allows adding the species now
	for (auto& str : { x->reactant() , x->product() }) {
		if (!has_species(str)) {
			add_blank_species(str);
			lib::normal_statement("To add the Xsec with process [" + x->process() + "] to the library, the species with name [" + str + "] was also added.");
		}
	}

	auto ptr = get_species(x->reactant());
	ptr->add_Xsec(x);

	lib::debug_statement("Xsec with process [" + x->process() + "] added to the library.");


	return;
	
}

// 11/18/2022
// overload
void lib::Library::add_Xsec(lib::FunctionalXsec x) {
	this->add_Xsec(std::make_shared<lib::FunctionalXsec>(x));
	return;
}



// 11/17/2022
// merge contents of one library into another
void lib::Library::add_Xsecs_from_library(lib::Library this_lib) {

	for (auto& spec : this_lib.allspecies) {
		for (auto& x : spec->allcollisions) {

			add_Xsec(x);

		}
	}
	
	return;

}


#endif