// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#ifndef COLLISIONLIBRARY_BORNAPPROX_ROT
#define COLLISIONLIBRARY_BORNAPPROX_ROT

#include "collisionlibrary"



// Analytic cross-section for dipole rotational collisions in linear molecules (via Born approximation)
// J:		pre-collision rotational state
// Jprime:	post-collision rotational state
// species: Name of molecule this collision "belongs" to the population of (for naming convention)
// mu:		permanent dipole moment of molecule [e * a0]
// B:		rotational constant of molecule [eV]
// use_anisoScattering:	if true, use DCS scattering. If false, isotropic instead.
lib::FunctionalXsec linear_dipole_rotation_DCS(int J, int Jprime, std::string species, double mu, double B, bool use_anisoScattering=false) {

	if (abs(J - Jprime) != 1) {
		lib::error_statement("Dipole rotational collision cross sections can only be generated for |J-Jprime| = +/- 1");
	}
	if (J < 0 || Jprime < 0) {
		lib::error_statement("Cannot generate linear-molecule dipole rotational collision cross sections for [J, Jprime] < 0");
	}

	double g_J = 2.0 * J + 1.0; // degeneracy of state J
	double g_Jprime = 2.0 * Jprime + 1.0; // degeneracy of state Jprime

	// energy of state J
	std::function<double(int)> e_state = [B](int J) { return (B * J * (J + 1.0)); };

	// energy gained/lost by electron by collision
	double EV_THRESH = abs(e_state(J) - e_state(Jprime));

	// Cross-section constant unrelated to incident electron energy
	double sigma0 = (lib::A0 * mu) * (lib::A0 * mu) * lib::RYDBERG / EV_THRESH  * std::max(J,Jprime);

	std::function<double(double)> foo_rot = [J, Jprime, g_J, EV_THRESH, sigma0, mu](double eV) {

		if (eV <= EV_THRESH) { return 0.0; }

		double x = eV / EV_THRESH;

		// f is as it appears in Vialetto et al 2021, DOI: 10.1088/1361-6595/ac0a4d
		double f = 16.0 * lib::PI / 3.0 / x * std::log(sqrt(x) + sqrt(x - 1));
		return sigma0 / g_J * f;
		
	};

	std::function<double(double)> foo_sup = [EV_THRESH, sigma0, g_J, g_Jprime, foo_rot](double eV) {

		double x = eV / EV_THRESH;

		double fsup = 16.0 * lib::PI / 3.0 / x * std::log(sqrt(x + 1) + sqrt(x));
		return sigma0 / g_J * fsup; 
		// unintuitive - g_J swapped with gJprime by the time you get here. Introduced error in Klein-Rosseland equation.

		// Via Klein-Rosseland relation
		//double sigma = g_J / g_Jprime * (eV + EV_THRESH) / eV * foo_rot(eV + EV_THRESH);
		//double sigma = (g_J / g_Jprime) * (eV + EV_THRESH) / eV * foo_rot(eV + EV_THRESH); // obscure issue - ignore g_J affect here
		//return sigma;
	};

	bool is_exc = J < Jprime; // is_sup otherwise

	std::string reactant_name = species + "(J=" + std::to_string(J) + ")";
	std::string product_name = species + "(J=" + std::to_string(Jprime) + ")";

	lib::FunctionalXsec X;
	if (is_exc) {

		X = lib::FunctionalXsec(foo_rot, lib::CollisionCode::excitation);
		X.eV_thresh(EV_THRESH);
		X.process("E + " + reactant_name + " -> E + " + product_name + ", Excitation (Born-approx, dipole rotation, linear-molecule)");
		
	}
	else {

		X = lib::FunctionalXsec(foo_sup, lib::CollisionCode::superelastic);
		X.parent_exc_thresh(EV_THRESH);
		X.g(g_Jprime / g_J);
		X.process("E + " + reactant_name + " -> E + " + product_name + ", Superelastic  (Born-approx, dipole rotation, linear-molecule)");
		
	}

	X.reactant(reactant_name);
	X.product(product_name);

	if (use_anisoScattering) {
		X.scattering(lib::dipole_BornApprox_scattering());

		arma::colvec eV = arma::logspace(-3, 3, 20);
		arma::colvec eVprime;
		// plus or minus a reasonable threshold energy for a rotation, to test
		if (is_exc) { eVprime = eV - 5e-4; }
		else { eVprime = eV + 5e-4; }
		
		X.scattering()->find_better_grid_chi(eV, eVprime); // necessary: linear spaced chi-grid usually behaves poorly
	}
	else {
		X.scattering(lib::isotropic_scattering());
	}

	return X;
}


// Analytic cross-section for quadrupole rotational collisions in linear molecules (via Born approximation)
// J:		pre-collision rotational state
// Jprime:	post-collision rotational state
// species: Name of molecule this collision "belongs" to the population of (for naming convention)
// Q:		permanent quadrupole moment of molecule [e * a0^2]
// B:		rotational constant of molecule [eV]
lib::FunctionalXsec linear_quadrupole_rotation_DCS(int J, int Jprime, std::string species, double Q, double B) {

	if (abs(J - Jprime) != 2) {
		lib::error_statement("Dipole rotational collision cross sections can only be generated for |J-Jprime| = +/- 2");
	}
	if (J < 0 || Jprime < 0) {
		lib::error_statement("Cannot generate linear-molecule dipole rotational collision cross sections for [J, Jprime] < 0");
	}

	double g_J = 2.0 * J + 1.0; // degeneracy of state J
	double g_Jprime = 2.0 * Jprime + 1.0; // degeneracy of state Jprime

	// energy of state J
	std::function<double(int)> e_state = [B](int J) { return (B * J * (J + 1.0)); };

	// energy gained/lost by electron by collision
	double EV_THRESH = abs(e_state(J) - e_state(Jprime));

	// Cross-section constant unrelated to incident electron energy
	double sigma0 = 8.0 * lib::PI / 15.0 * (lib::A0 * Q) * (lib::A0 * Q);

	std::function<double(double)> foo_rot = [J, Jprime, EV_THRESH, sigma0, Q](double eV) {

		if (eV <= EV_THRESH) { return 0.0; }

		// kb_ka is equivalent to how it appears per Gerjuoy and Stein 1955 DOI: 10.1103/PhysRev.97.1671
		double kb_ka = std::sqrt(1.0 - EV_THRESH / eV);
		double sigma = sigma0 * kb_ka * (J + 2.0) * (J + 1.0) / (2.0 * J + 3.0) / (2.0 * J + 1);

		return sigma;
	};

	std::function<double(double)> foo_sup = [sigma0, J, EV_THRESH, g_J, g_Jprime, foo_rot](double eV) {

		// kb_ka is equivalent to how it appears per Gerjuoy and Stein 1955 DOI: 10.1103/PhysRev.97.1671
		double kb_ka = std::sqrt(1.0 + EV_THRESH / eV);
		double sigma = sigma0 * kb_ka * (J) * (J - 1.0) / (2.0 * J - 1.0) / (2.0 * J + 1);
		return sigma;

	};

	bool is_exc = J < Jprime; // is_sup otherwise

	std::string reactant_name = species + "(J=" + std::to_string(J) + ")";
	std::string product_name = species + "(J=" + std::to_string(Jprime) + ")";

	lib::FunctionalXsec X;
	if (is_exc) {
		X = lib::FunctionalXsec(foo_rot, lib::CollisionCode::excitation);
		X.eV_thresh(EV_THRESH);
		X.process("E + " + reactant_name + " -> E + " + product_name + ", Excitation (Born-approx, quadrupole rotation, linear-molecule)");
	}
	else {
		X = lib::FunctionalXsec(foo_sup, lib::CollisionCode::superelastic);
		X.parent_exc_thresh(EV_THRESH);
		X.g(g_Jprime / g_J);
		X.process("E + " + reactant_name + " -> E + " + product_name + ", Superelastic  (Born-approx, quadrupole rotation, linear-molecule)");
	}

	X.reactant(reactant_name);
	X.product(product_name);
	X.scattering(lib::isotropic_scattering());

	return X;
}

#endif