%--------- Remap the energy grid --------%
u_max_prev = u_max;
if ( f_0(round(0.875*Nu)) < 1E-12) && iter1 > 3
    [f_0u indf] = unique(f_0);
    u_max = interp1(f_0u,ue(indf),1E-10,'linear','extrap');
    u_max = max(u_max,5*qe);
    Du = u_max/(Nu-1);      % Magnitude of the energy step in the finite difference scheme
    uo(uv) = (uv)*Du;       % Vector of energy points for the l='odd' grid 
    ue(uv) = (uv-1/2)*Du;   % Vector of energy points for the l='even' grid            for Ng = 1:N_gases
elseif f_0(round(0.875*Nu)) > 1E-8 && iter1 > 3
    u_max = 1.25*u_max; % Determine the new upper limit of the energy grid
    Du = u_max/(Nu-1);      % Magnitude of the energy step in the finite difference scheme
    uo(uv) = (uv)*Du;       % Vector of energy points for the l='odd' grid 
    ue(uv) = (uv-1/2)*Du;   % Vector of energy points for the l='even' grid            for Ng = 1:N_gases
end             

if u_max ~= u_max_prev
    REMAPPED = 1;
    for Ng = 1:N_gases      % Interpolate cross-sections to the new energy grids 
        fid = fopen(string(Xsec_fids(Ng)));     
        Read_Xsecs          % Run the cross-sections import script (Reads the LXCat file, and interpolates the Xsecs to the numerical grid)
    end
    s_Te_eff(uv) = 0;
    for Ng = 1:N_gases
        s_Te_eff(uv) = s_Te_eff(uv) + squeeze(s_Te_gas(uv,Ng))';
    end
end