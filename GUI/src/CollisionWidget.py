import os
import configparser
from LXCat_tools import *

import const as c

##--------------------------------


from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QLineEdit, QGraphicsLayoutItem, QGroupBox, QFormLayout, QPushButton, QComboBox, QTableWidgetItem, QWidget, QListWidget, QMainWindow, QHBoxLayout, QCheckBox, QLabel, QLineEdit
from PyQt5.QtWidgets import QFileDialog, QVBoxLayout, QTableWidget, QCheckBox, QTabWidget


interp_variables_dir = {"Linear" : 0,
                "Logarithmic" : 1}


class ButtonsFrame(QWidget):
        def __init__(self, parent=None):
            super(QWidget, self).__init__()
            self.home()
            self.connects()
            self.layout()
            

        def home(self):
            
            self.addButton = QPushButton("Add File")
            self.clearButton = QPushButton("Clear All")
            self.selectAllButton = QPushButton("Select All")
            self.selectNoneButton = QPushButton("Select None")

        def connects(self):
            pass

        def layout(self):
            layout = QVBoxLayout()
            #layout.addWidget(self.addDefaultCombo)
            grouplay = QVBoxLayout()
            group = QGroupBox()

            grouplay.addWidget(self.addButton)
            grouplay.addWidget(self.clearButton)
            grouplay.addWidget(QLabel())
            grouplay.addWidget(self.selectAllButton)
            grouplay.addWidget(self.selectNoneButton)
            group.setLayout(grouplay)

            layout.addWidget(group)

            self.setLayout(layout)



class TitledList(QWidget):
    def __init__(self, title="NoTitle", parent=None):
        super(QWidget, self).__init__()
        self.title = title
        
        self.home()
        self.connects()
        self.layout()

    def home(self):
        self.ls = QListWidget(self)
        self.ls.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)

        self.ls.setMinimumWidth(QLabel().width())

    def connects(self):
        pass

    def layout(self):
        layout = QVBoxLayout()

        #group = QGroupBox(self.title)
        #layout.addWidget(QLabel(self.title))
        #grouplay = QVBoxLayout()
        layout.addWidget(self.ls)
        #group.setLayout(grouplay)

        #layout.addWidget(group)
        self.setLayout(layout)  

    def has_entry(self, entry):
        for i in range(self.ls.count()):
            if self.ls.item(i).text() == entry:
                return True
        return False  
    
    def add_entry(self, entry):
        if not self.has_entry(entry):
            self.ls.addItem(entry)


class CollisionsSection(QWidget):
    def __init__(self, title="NoTitle", parent=None):
        super(QWidget, self).__init__()
        self.title = title

        self.home()
        self.connects()
        self.layout()

    def home(self):

        self.table = QTableWidget(self)
        self.table.setRowCount(0)
        self.table.setColumnCount(2)

        self.table.setColumnWidth(0, int(QLabel().width() * 0.95))
        self.table.setColumnWidth(1, int(QCheckBox().width() / 4))

        self.table.setHorizontalHeaderLabels(["Process", "Scale"])
        self.table.verticalHeader().setVisible(False)

        # self.table.horizontalScrollBar().hide()

        self.table.setMinimumWidth(QLabel().width())

    def connects(self):
        pass

    def layout(self):
        layout = QVBoxLayout()

        # group = QGroupBox(self.title)
        # layout.addWidget(QLabel(self.title))
        # grouplay = QVBoxLayout()
        #layout.addWidget(self.ls)
        # group.setLayout(grouplay)
        layout.addWidget(self.table)

        # layout.addWidget(group)
        self.setLayout(layout)

    def has_entry(self, proc):
        for i in range(self.table.rowCount()):
            if self.table.item(i, 0).text() == proc:
                return True

        return False

    def add_entry(self, proc):
        if not self.has_entry(proc):
            N = self.table.rowCount()
            self.table.insertRow(N)

            stritem = QTableWidgetItem(proc)
            #stritem.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
            #stritem.setCheckState(QtCore.Qt.Unchecked)


            scaleitem = QTableWidgetItem("1.0")

            self.table.setItem(N, 0, stritem)
            self.table.setItem(N, 1, scaleitem)

    def get_all_processes(self):
        procs = []
        for i in range(self.table.rowCount()):
            item = self.table.item(i, 0)
            procs.append(item.text())
        return procs

    def get_all_process_scales(self):
        scales = []
        for i in range(self.table.rowCount()):
            item = self.table.item(i, 1)
            scales.append(item.text())
        return scales


class SpeciesSection(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()

    def home(self):
        self.table = QTableWidget(self)
        self.table.setRowCount(0)
        self.table.setColumnCount(2)

        self.table.setColumnWidth(0, int(QLabel().width() * 0.95))
        self.table.setColumnWidth(1, int(QCheckBox().width() / 4))

        self.table.setHorizontalHeaderLabels(["Species Name", "[x/1]"])
        self.table.verticalHeader().setVisible(False)

        #self.table.horizontalScrollBar().hide()

        self.table.setMinimumWidth(QLabel().width() )


    def connects(self):
        pass

    def layout(self):
        layout = QVBoxLayout()

        #group = QGroupBox("Species")
        #grouplay = QVBoxLayout()
        layout.addWidget(self.table)
        #group.setLayout(grouplay)

        #layout.addWidget(group)
        self.setLayout(layout) 

    def has_entry(self, spec):
        for i in range(self.table.rowCount()):
            if self.table.item(i, 0).text() == spec:
                return True

        return False

    def add_species(self, spec):
        if not self.has_entry(spec):
            N = self.table.rowCount()
            self.table.insertRow(N)

            stritem = QTableWidgetItem(spec)
            stritem.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
            stritem.setCheckState(QtCore.Qt.Unchecked)

            if N == 0:
                fracitem = QTableWidgetItem("1.0")
            else:
                fracitem = QTableWidgetItem("0.0")

            self.table.setItem(N, 0, stritem)
            self.table.setItem(N, 1, fracitem)

    def select_species(self, specs):
        for i in range(self.table.rowCount()):
            item = self.table.item(i, 0)
            for s in specs:
                if item.text() == s:
                    item.setCheckState(QtCore.Qt.Checked)

    def select_all(self):
        for i in range(self.table.rowCount()):
            item = self.table.item(i, 0)
            item.setCheckState(QtCore.Qt.Checked)

    def select_none(self):
        for i in range(self.table.rowCount()):
            item = self.table.item(i, 0)
            item.setCheckState(QtCore.Qt.Unchecked)

    def get_all_selected_species(self):
        specs = []
        for i in range(self.table.rowCount()):
            item = self.table.item(i, 0)
            if item.checkState() == QtCore.Qt.Checked:
                specs.append(item.text())
        return specs

    def get_all_selected_fracs(self):
        fracs = []
        for i in range(self.table.rowCount()):
            item = self.table.item(i, 0)
            if item.checkState() == QtCore.Qt.Checked:
                fracs.append(self.table.item(i, 1).text())
        return fracs

    def assign_frac_to_spec(self, frac, spec):
        for i in range(self.table.rowCount()):
            item = self.table.item(i, 0)
            if item.text() == spec:
                self.table.item(i, 1).setText(frac)


class ScatteringSection(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()

    def home(self):

        self.elasticScatter = QComboBox(self)
        self.elasticScatter.addItem("Isotropic")
        self.elasticScatter.addItem("ScreenedCoulomb")

        self.excitationScatter = QComboBox(self)
        self.excitationScatter.addItem("Isotropic")
        self.excitationScatter.addItem("IdealForward")
        self.excitationScatter.addItem("ScreenedCoulomb")

        self.ionizationScatter = QComboBox(self)
        self.ionizationScatter.addItem("Isotropic")
        self.ionizationScatter.addItem("IdealForward")
        self.ionizationScatter.addItem("ScreenedCoulomb")

        self.superelasticScatter = QComboBox(self)
        self.superelasticScatter.addItem("Isotropic")
        self.superelasticScatter.addItem("IdealForward")
        self.superelasticScatter.addItem("ScreenedCoulomb")

    def connects(self):
        pass

    def layout(self):
        

        layout = QFormLayout()
        layout.addRow("Elastic", self.elasticScatter)
        layout.addRow("Excitation", self.excitationScatter)
        layout.addRow("Ionization", self.ionizationScatter)
        layout.addRow("Superelastic", self.superelasticScatter)
        group = QGroupBox("Scattering")
        #group.setLayout(layout)
        #layout.addWidget(group)

        self.setLayout(layout) 


class MoreSection(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()

    def home(self):

        self.sharing = QLineEdit(self)
        self.DONT_ENFORCE_SUM = QCheckBox(self)
        #self.screening = QLineEdit(self)
        self.interp_method = QComboBox(self)
        self.interp_method.addItem("Linear")
        self.interp_method.addItem("Logarithmic")
        


    def connects(self):
        pass

    def layout(self):
        

        layout = QFormLayout()
        layout.addRow("Electron Energy Sharing [0, 0.5]", self.sharing)
        layout.addWidget(QLabel("Note: 0.5 creates equal-sharing, 0 creates one-takes-all."))
        layout.addRow("Allow sum(fracs) != 1: ", self.DONT_ENFORCE_SUM)
        #layout.addRow("Screening Energy [eV] (ScreenedCoulomb only)", self.screening) # technically unvalidated, do not allow yet
        layout.addRow("Interpolation method: ", self.interp_method)

        self.setLayout(layout) 

#####----------------------------------------------------------------###
class CollisionWidget(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()
        self.set_defaults()
        

    def home(self):
        self.buttonsFrame = ButtonsFrame(self)
        self.filesSection = TitledList("Cross section filenames", self)
        self.speciesSection = SpeciesSection(self)
        self.collisionsSection = CollisionsSection("Collisions", self)
        self.scatteringSection = ScatteringSection(self)
        self.moreSection = MoreSection(self)
        


    def connects(self):
        self.buttonsFrame.addButton.clicked.connect(self.add_files)
        self.buttonsFrame.clearButton.clicked.connect(self.clear_all)

        self.filesSection.ls.model().rowsInserted.connect(self.add_species_from_file)
        self.filesSection.ls.model().rowsInserted.connect(self.add_collisions_from_file)


        self.buttonsFrame.selectAllButton.clicked.connect(self.speciesSection.select_all)
        self.buttonsFrame.selectNoneButton.clicked.connect(self.speciesSection.select_none)
        

    def layout(self):

        layout = QVBoxLayout()

        hbox = QHBoxLayout()
        

        group = QGroupBox("LXCat Cross Section Files")
        hbox.addWidget(self.buttonsFrame)
        hbox.addWidget(self.filesSection)
        group.setLayout(hbox)
        
        

        layout.addWidget(group)    


        tabs = QTabWidget()
        tabs.addTab(self.speciesSection, "Species")
        tabs.addTab(self.collisionsSection, "Collision")
        tabs.addTab(self.scatteringSection, "Scattering")
        tabs.addTab(self.moreSection, "More")

        layout.addWidget(tabs)


        self.setLayout(layout)
    

    def add_files(self):
        options = QFileDialog.Options()
        files, _ = QFileDialog.getOpenFileNames(self,"QFileDialog.getOpenFileNames()", "","All Files (*);;Python Files (*.py)", options=options)
        if files:
            for filename in files:
                self.filesSection.add_entry(filename)


    def clear_all(self):
        self.filesSection.ls.clear()
        self.speciesSection.table.setRowCount(0)
        self.collisionsSection.table.setRowCount(0)


    

    def add_species_from_file(self, modelIdx, place):
        # in this case, first and last always the same number.

        f = self.filesSection.ls.item(place).text()
        specs = find_species_in_LXCat(f)
        for spec in specs:
            self.speciesSection.add_species(spec)
            
 
    

    def add_collisions_from_file(self, modelIdx, place):
        # in this case, first and last always the same number.

        f = self.filesSection.ls.item(place).text()
        procs = find_processes_in_LXCat(f)
        for proc in procs:
            self.collisionsSection.add_entry(proc)



    def get_args(self):

        #print("argtest")
        #return
        fracs = self.speciesSection.get_all_selected_fracs()
        specs = self.speciesSection.get_all_selected_species()

        procs = self.collisionsSection.get_all_processes()
        scales = self.collisionsSection.get_all_process_scales()

        args = ""
        for i in range(self.filesSection.ls.count()):
            args = args + " --LXCat_Xsec_fid \"{}\" ".format(self.filesSection.ls.item(i).text())

        for i in range(len(fracs)):
            args = args + " --species \"{}\" ".format(specs[i])
                
            try:
                float(fracs[i]);
                args = args + " {} ".format(fracs[i])
            except:
                args = args + " ERROR-(float(frac)) ";

        for i in range(len(procs)):
            if scales[i] != "1.0" and scales[i] != "1": # only do this if the scale is something not-typical
                args = args + " --scale_Xsec \"{}\" ".format(procs[i])

                try:
                    float(scales[i]);
                    args = args + " {} ".format(scales[i])
                except:
                    args = args + " ERROR-(float(scale)) ";


        args = args + " --elastic_scattering {} ".format(self.scatteringSection.elasticScatter.currentText()) 
        args = args + " --excitation_scattering {} ".format(self.scatteringSection.excitationScatter.currentText()) 
        args = args + " --ionization_scattering {} ".format(self.scatteringSection.ionizationScatter.currentText()) 
        args = args + " --superelastic_scattering {} ".format(self.scatteringSection.superelasticScatter.currentText()) 
        
        args = args + " --sharing "
        try:
            float(self.moreSection.sharing.text());
            args = args + " {} ".format(self.moreSection.sharing.text())
        except:
            args = args + " ERROR-(float(sharing)) ";

        if self.moreSection.DONT_ENFORCE_SUM.isChecked():
            args = args + " --DONT_ENFORCE_SUM ";

        args = args + " --interp_method {} ".format(self.moreSection.interp_method.currentText()) 
        

        return args



    def set_defaults(self):
        try:
            config = configparser.ConfigParser()
            config.read(c.ini_name)
            default = config['Default']

            self.clear_all()

            fids = default["LXCat_Xsec_fid"].split()
            for f in fids:
                #print(default["default_Xsec_location"] + "/" + f)
                path = str(c.XSEC_PATH + f).replace("\\", "/")
                self.filesSection.add_entry(path)

            args = default["species"].split()
            specs = []
            fracs = []
            for i in range(0, len(args)-1, 2):
                specs.append(args[i])
                fracs.append(args[i+1])

            
            self.speciesSection.select_species(specs)
            for i in range(len(specs)):
                self.speciesSection.assign_frac_to_spec(fracs[i], specs[i])



            self.scatteringSection.elasticScatter.setCurrentText(default["elastic_scattering"])
            self.scatteringSection.excitationScatter.setCurrentText(default["excitation_scattering"])
            self.scatteringSection.ionizationScatter.setCurrentText(default["ionization_scattering"])
            self.scatteringSection.superelasticScatter.setCurrentText(default["superelastic_scattering"])

            self.moreSection.sharing.setText(default["sharing"])

            if default["DONT_ENFORCE_SUM"] == "Yes":
                self.moreSection.DONT_ENFORCE_SUM.setChecked(True)
            else:
                self.moreSection.DONT_ENFORCE_SUM.setChecked(False)

            self.moreSection.interp_method.setCurrentIndex(interp_variables_dir[default["interp_method"]])
            #self.moreSection.interp_method.setText(default["interp_method"])

            
        except:
            return

        


if __name__ == "__main__":
    import sys

    app = QApplication(sys.argv)
    window = QMainWindow()
    x = CollisionWidget(window)
    x.show()

    sys.exit(app.exec_())

