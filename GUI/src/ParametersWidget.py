import configparser
import os
import time

from PyQt5.QtWidgets import QApplication, QPushButton, QWidget, QMainWindow, QCheckBox, QLabel, QLineEdit
from PyQt5.QtWidgets import QGroupBox, QFileDialog, QComboBox, QTextEdit, QButtonGroup, QVBoxLayout, QCheckBox, QFormLayout, QTabWidget, QRadioButton
from PyQt5.QtCore import Qt

class ModelParamsWidget(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()

    def home(self):
        self.option = QButtonGroup(self)
        self.option.setExclusive(True)

        self.SST = QRadioButton(self)
        self.HD = QRadioButton(self)
        self.HDGE = QRadioButton(self)

        self.option.addButton(self.SST, 0)
        self.option.addButton(self.HD, 1)
        self.option.addButton(self.HDGE, 2)

        self.terms = QLineEdit(self)

        

    def connects(self):
        pass
    
    def layout(self):

        layout = QVBoxLayout()

        form = QFormLayout()
        form.addRow("SST", self.SST)
        form.addRow("HD", self.HD)
        form.addRow("HD + GE", self.HDGE)
        form.addRow("Terms [#]", self.terms)
        group = QGroupBox("Model")
        group.setLayout(form)
        layout.addWidget(group)

        

        self.setLayout(layout)
        
        pass
    
    def get_args(self):
        args = ""
        
        if self.SST.isChecked():
            args = args + "--model SST "
        elif self.HD.isChecked():
            args = args + "--model HD "
        elif self.HDGE.isChecked():
            args = args + "--model HD+GE "

        try: args = args + "--N_terms {} ".format(int(self.terms.text()))
        except: args = args + "ERROR-int(N_terms) "

        return args
    

class EnvironParamsWidget(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()

    def home(self):
        self.EN_Td = QLineEdit(self)
        self.pressure = QLineEdit(self)
        self.temperature = QLineEdit(self)

    def connects(self):
        pass
    
    def layout(self):

        layout = QVBoxLayout()

        

        form = QFormLayout()
        form.addRow(QLabel("Field Strength [Td]"), self.EN_Td)
        form.addRow(QLabel("Pressure [Torr]"), self.pressure)
        form.addRow(QLabel("Temperature [K]"), self.temperature)
        group = QGroupBox("Environment")
        group.setLayout(form)

        layout.addWidget(group)

        self.setLayout(layout)
    
    def get_args(self):
        args = ""
        
        try: float(self.pressure.text()); args = args + "--p_Torr {} ".format(self.pressure.text())
        except: args = args + "ERROR-float(p_Torr) "

        try: float(self.temperature.text()); args = args + "--T_K {} ".format(self.temperature.text())
        except: args = args + "ERROR-float(T_K) "

        try: float(self.EN_Td.text()); args = args + "--EN_Td {} ".format(self.EN_Td.text())
        except: args = args + "ERROR-float(EN_Td) "

        return args
    
    

class NumericParamsWidget(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()

    def home(self):
        self.points = QLineEdit(self)
        self.max_eV = QLineEdit(self)
        self.USE_EV_MAX_GUESS = QCheckBox(self)
        self.USE_ENERGY_REMAP = QCheckBox(self)
        self.USE_NU_REMAP = QCheckBox(self);
        self.target_log_height = QLineEdit(self)
        self.remap_allowance = QLineEdit(self)
        self.max_trials = QLineEdit(self)
        self.remap_Nu_increment = QLineEdit(self)
        self.remap_Nu_max = QLineEdit(self)
        pass

    def connects(self):
        self.USE_EV_MAX_GUESS.stateChanged.connect(self.toggle_max_eV)
        pass
    
    def layout(self):
        layout = QVBoxLayout()

        form = QFormLayout()
        #layout.addWidget(QLabel("- Numeric -"))
        form.addRow("Grid Points [#]", self.points)
        form.addRow("Max Energy [eV]", self.max_eV)
        #form.addRow(QLabel())
        form.addRow("Use Max Energy Guess?", self.USE_EV_MAX_GUESS)
        form.addRow("Allow Energy Max Remap?", self.USE_ENERGY_REMAP)
        form.addRow("Allow Grid Points Remap?", self.USE_NU_REMAP)
        group = QGroupBox("Numeric Grid Settings")
        group.setLayout(form)
        
        # moved to different section
        #layout.addWidget(QLabel("- Remap Settings -"))
        form2 = QFormLayout()
        form2.addRow("Max Remap Trials", self.max_trials)
        form2.addRow("Target EEDF Decay", self.target_log_height)
        form2.addRow("Decay Allowance", self.remap_allowance)
        form2.addRow("Max Remapped Grid Points", self.remap_Nu_max)
        form2.addRow("Grid Points Increment", self.remap_Nu_increment)
        group2 = QGroupBox("Grid Remap Settings")
        group2.setLayout(form2)

        layout.addWidget(group)
        layout.addWidget(group2)

        self.setLayout(layout)
        pass

    def toggle_max_eV(self, past):
        if past == Qt.Unchecked:
            self.max_eV.clear()
            self.max_eV.setEnabled(True)
        else:
            self.max_eV.setText("100")
            self.max_eV.setEnabled(False)
        
    def get_args(self):
        args = ""
        
        try: args = args + "--Nu {} ".format(int(self.points.text()))
        except: args = args + "ERROR-int(Nu) "

        try: float(self.max_eV.text()); args = args + "--initial_eV_max {} ".format(self.max_eV.text())
        except: args = args + "ERROR-float(initial_eV_max) "

        if self.USE_ENERGY_REMAP.isChecked():
            args = args + "--USE_ENERGY_REMAP "

        if self.USE_EV_MAX_GUESS.isChecked():
            args = args + "--USE_EV_MAX_GUESS "

        if self.USE_NU_REMAP.isChecked():
            args = args + "--USE_NU_REMAP "

        try: float(self.target_log_height.text()); args = args + "--remap_target_order_span {} ".format(self.target_log_height.text())
        except: args = args + "ERROR-float(target_order_span) "

        try: float(self.remap_allowance.text()); args = args + "--remap_allowance {} ".format(self.remap_allowance.text())
        except: args = args + "ERROR-float(remap_allowance) "

        try: args = args + "--remap_grid_trial_max {} ".format(int(self.max_trials.text()))
        except: args = args + "ERROR-int(grid_trial_max) "

        try: float(self.remap_Nu_max.text()); args = args + "--remap_Nu_max {} ".format(self.remap_Nu_max.text())
        except: args = args + "ERROR-float(remap_Nu_max) "

        try: float(self.remap_Nu_increment.text()); args = args + "--remap_Nu_increment {} ".format(self.remap_Nu_increment.text())
        except: args = args + "ERROR-float(remap_Nu_increment) "

        return args



class MoreParamsWidget(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()

    def home(self):
        self.converr = QLineEdit(self)
        self.weight = QLineEdit(self)
        self.max_iter = QLineEdit(self)
        self.min_iter = QLineEdit(self)
        self.multibolt_num_threads = QLineEdit(self)

        self.SHY = QCheckBox(self)
        self.SILENT = QCheckBox(self)
        #self.default_settings = QPushButton("Use Defaults", self)
        #self.clear_settings = QPushButton("Clear Settings", self)
        pass

    def connects(self):
        pass
    
    def layout(self):
        layout = QVBoxLayout()

        form = QFormLayout()
        #layout.addWidget(QLabel("- Iterations -"))
        form.addRow("Conv. Error", self.converr)
        form.addRow("Relax. Weight", self.weight)
        form.addRow("Max Iter.", self.max_iter)
        form.addRow("Min Iter.", self.min_iter)
        group = QGroupBox("Iterations")
        group.setLayout(form)
        layout.addWidget(group)
        
        
        form  = QFormLayout()
        form.addRow("Threads", self.multibolt_num_threads)
        group = QGroupBox("Parallelization")
        group.setLayout(form)
        layout.addWidget(group)

        form  = QFormLayout()
        form.addRow("'Shyness': Quit failing calculations early", self.SHY)
        form.addRow("Suppress all exe output statements", self.SILENT)
        group = QGroupBox("Misc.")
        group.setLayout(form)
        layout.addWidget(group)
        
        self.setLayout(layout)
    

    def get_args(self):
        args = ""
        #caveat: you want to return the string of the float as-is rather than translate then pass

        try: float(self.converr.text()); args = args + "--conv_err {} ".format(self.converr.text())
        except: args = args + "ERROR-float(conv_err) "

        try: float(self.weight.text()); args = args + "--weight_f0 {} ".format(self.weight.text())
        except: args = args + "ERROR-float(weight_f0) "

        try: args = args + "--iter_max {} ".format(int(self.max_iter.text()))
        except: args = args + "ERROR-int(iter_max) "

        try: args = args + "--iter_min {} ".format(int(self.min_iter.text()))
        except: args = args + "ERROR-int(iter_min) "


        try:
            if self.multibolt_num_threads.text() != "max" :
                args = args + "--multibolt_num_threads {} ".format(int(self.multibolt_num_threads.text()))
            else:
                args = args + "--multibolt_num_threads max "
        except: args = args + "ERROR-int(multibolt_num_threads) "
        
        if self.SHY.isChecked():
            args = args + " --SHY "

        if self.SILENT.isChecked():
            args = args + " --SILENT "


        return args




class ExportParamsWidget(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()

    def home(self):
        self.allow = QCheckBox(self)
        self.export_Xsecs = QCheckBox(self);
        self.button = QPushButton("Change Export Location", self)
        self.location = QLineEdit(self)
        self.location.setReadOnly(True)
        self.export_name = QLineEdit(self)
        #self.name = QLineEdit(self)

    def connects(self):
        self.button.clicked.connect(self.change_dir)
        pass
    
    def layout(self):
        layout = QVBoxLayout()

        form = QFormLayout()
        form.addRow("Suppress Export?", self.allow)
        form.addRow(QLabel(""))
        form.addRow("Export Xsecs?", self.export_Xsecs)
        form.addRow(QLabel(""))
        form.addRow(self.button, QLabel(""))
        form.addRow(self.location)
        form.addRow(QLabel(""))
        form.addRow(QLabel("Exports with name (blank for Run_*):"))
        form.addRow(self.export_name)
        group = QGroupBox("Export Settings")
        group.setLayout(form)
        layout.addWidget(group)

        self.setLayout(layout)
        pass

    def change_dir(self):
        filename = QFileDialog.getExistingDirectory(self, 'Open Dir', './')
        if filename == "":
            return

        self.location.setText(filename)
        pass

    def get_args(self):
        args = ""

        if self.allow.isChecked():
            args = args + "--NO_EXPORT "
            return args

        if self.export_Xsecs.isChecked():
            args = args + "--EXPORT_XSECS "


        args = args + "--export_location \"{}\" ".format(self.location.text())


        if self.export_name.text().strip() != "":
            try:
                with open(self.export_name.text(), 'w'): # if is valid name, this will not trip
                    pass
                os.remove(self.export_name.text())


                args = args + "--export_name \"{}\" ".format(self.export_name.text())

            except:
                args = args + "--export_name ERROR-invalidFilename(export_name) ".format(self.export_name.text())



        return args


class ParamsWidget(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()

        self.set_defaults()

    def home(self):

        self.modelSection = ModelParamsWidget(self)
        self.environSection = EnvironParamsWidget(self)
        self.numericSection = NumericParamsWidget(self)
        self.moreSection = MoreParamsWidget(self)
        #self.argsSection = ArgsWidget(self)
        self.exportSection = ExportParamsWidget(self)
        #self.sweep = SweepParamsWidget(self)

    def connects(self):
        pass
    
    def layout(self):
        vboxgen = QVBoxLayout()
        vboxgen.addWidget(self.modelSection)
        vboxgen.addWidget(self.environSection)


        vboxnum = QVBoxLayout()
        vboxnum.addWidget(self.numericSection)

        vboxmore = QVBoxLayout()
        vboxmore.addWidget(self.moreSection)

        #vboxargs = QVBoxLayout()
        #vboxargs.addWidget(self.argsSection)

        vboxexp = QVBoxLayout()
        vboxexp.addWidget(self.exportSection)

        gentab = QWidget()
        gentab.setLayout(vboxgen)

        numtab = QWidget()
        numtab.setLayout(vboxnum)

        moretab = QWidget()
        moretab.setLayout(vboxmore)

        #argstab = QWidget()
        #argstab.setLayout(vboxargs)

        exporttab = QWidget()
        exporttab.setLayout(vboxexp)

        tabs = QTabWidget(self)
        tabs.addTab(gentab, "General")
        tabs.addTab(numtab, "Numeric")
        tabs.addTab(exporttab, "Export")
        tabs.addTab(moretab, "More")
        #tabs.addTab(argstab, "Args.")
        
        

        layout = QVBoxLayout(self)
        layout.addWidget(tabs)
        self.setLayout(layout)

        #tabs.setMaximumWidth(tabs.sizeHint().width())
        
    def get_args(self):
        args = ""
        args = args + self.modelSection.get_args()
        args = args + self.environSection.get_args()
        args = args + self.numericSection.get_args()
        args = args + self.moreSection.get_args()
        #args = args + self.argsSection.get_args()
        args = args + self.exportSection.get_args()

        return args


    def set_defaults(self):
        try:
            config = configparser.ConfigParser()
            config.read("config.ini")
            default = config['Default']
            
            model = default["model"]
            if model == "SST":
                self.modelSection.SST.setChecked(True)
            elif model == "HD":
                self.modelSection.HD.setChecked(True)
            elif model == "HD+GE":
                self.modelSection.HDGE.setChecked(True)

            self.modelSection.terms.setText(default["N_terms"])

            self.environSection.EN_Td.setText(default["EN_Td"])
            self.environSection.pressure.setText(default["p_Torr"])
            self.environSection.temperature.setText(default["T_K"])

            self.numericSection.points.setText(default["Nu"])
            self.numericSection.max_eV.setText(default["initial_eV_max"])

            if default["USE_EV_MAX_GUESS"] == "Yes":
                self.numericSection.USE_EV_MAX_GUESS.setCheckState(Qt.Checked)
            else:
                self.numericSection.USE_EV_MAX_GUESS.setCheckState(Qt.Unchecked)

            if default["USE_ENERGY_REMAP"] == "Yes":
                self.numericSection.USE_ENERGY_REMAP.setCheckState(Qt.Checked)
            else:
                self.numericSection.USE_ENERGY_REMAP.setCheckState(Qt.Unchecked)

            if default["USE_NU_REMAP"] == "Yes":
                self.numericSection.USE_NU_REMAP.setCheckState(Qt.Checked)
            else:
                self.numericSection.USE_NU_REMAP.setCheckState(Qt.Unchecked)

            

            self.numericSection.target_log_height.setText(default["remap_target_order_span"])

            self.numericSection.remap_allowance.setText(default["remap_allowance"])

            self.numericSection.max_trials.setText(default["remap_grid_trial_max"])

            self.numericSection.remap_Nu_max.setText(default["remap_Nu_max"])
            
            self.numericSection.remap_Nu_increment.setText(default["remap_Nu_increment"])


            if default["NO_EXPORT"] == "Yes":
                self.exportSection.allow.setCheckState(Qt.Checked)
            else:
                self.exportSection.allow.setCheckState(Qt.Unchecked)

            if default["EXPORT_XSECS"] == "Yes":
                self.exportSection.export_Xsecs.setCheckState(Qt.Checked)
            else:
                self.exportSection.export_Xsecs.setCheckState(Qt.Unchecked)

            self.exportSection.location.setText(default["default_export_location"])
            self.exportSection.export_name.setText(default["export_name"])

            self.moreSection.converr.setText(default["conv_err"])
            self.moreSection.weight.setText(default["weight_f0"])
            self.moreSection.max_iter.setText(default["iter_max"])
            self.moreSection.min_iter.setText(default["iter_min"])

            

            self.moreSection.multibolt_num_threads.setText(default["multibolt_num_threads"])

            if default["SILENT"] == "Yes":
                self.moreSection.SILENT.setCheckState(Qt.Checked)
            else:
                self.moreSection.SILENT.setCheckState(Qt.Unchecked)

            if default["SHY"] == "Yes":
                self.moreSection.SHY.setCheckState(Qt.Checked)
            else:
                self.moreSection.SHY.setCheckState(Qt.Unchecked)

        except:
            return

if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)
    window = QMainWindow()
    x = ParamsWidget(window)

    x.show()

    sys.exit(app.exec_())