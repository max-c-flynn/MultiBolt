print("Opening MultiBolt GUI...")


from multiprocessing import Process, freeze_support, Pool
import shutil
import subprocess
from subprocess import PIPE
import os

import const as c

MAX_CPU = os.cpu_count() # refer to prevent errors from calling too many processes





from ParametersWidget import ParamsWidget
from SweepWidget import SweepWidget
from ViewDataWidget import ViewDataWidget
from CollisionWidget import CollisionWidget
from ConsoleWidget import ConsoleWidget
from ArgsWidget import ArgsWidget

from PyQt5.QtWidgets import QApplication, QHBoxLayout, QMessageBox, QPushButton, QTableWidgetItem, QWidget, QMainWindow, QCheckBox, QLabel, QLineEdit
from PyQt5.QtWidgets import QMenuBar, QMenu, QVBoxLayout, QCheckBox, QFormLayout, QTabWidget, QRadioButton, QAction
from PyQt5.QtCore import QThreadPool, QRunnable, pyqtSlot, Qt
from PyQt5.QtGui import QColor, QLinearGradient, QPalette, QBrush



jobs = [] # stack for processes

class GUI(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        
        self.setWindowTitle("MultiBolt - A mutli-term Boltzmann equation solver for LTP")

        self.threadpool = QThreadPool(self) # pyqt pool for threads
        self.startSim_worker = Worker(self.attempt_calculation)

        self.home()
        self.connects()
        self.layout()
        
        self.set_defaults()

        # ----

        self.show()

        

     

        
        


    def home(self):
        self.params = ParamsWidget(self)
        self.collisions = CollisionWidget(self)
        self.sweep = SweepWidget(self)
        self.viewData = ViewDataWidget(self)
        self.console = ConsoleWidget(self)

        self.args = ArgsWidget(self)

        self.startButton = QPushButton("START")
        self.startButton.setStyleSheet("background-color: " + c.START_COLOR)
        

        self.stopButton = QPushButton("STOP")
        self.stopButton.setStyleSheet("background-color: " + c.RED_COLOR)
        self.stopButton.setEnabled(False)

        self.menuBar = QMenuBar()
        # Creating menus using a QMenu object
        self.defMenu = QMenu("&Settings")
        self.menuBar.addMenu(self.defMenu)

        self.defAction = QAction('Set Defaults', self)
        self.defMenu.addAction(self.defAction)



        pass

    def connects(self):
        self.args.writeButton.clicked.connect(lambda: self.args.txt.setText(self.get_args()))
        self.sweep.variable_choice.currentTextChanged.connect(self.handle_sweep_choice)
        self.handle_sweep_choice()

        self.startButton.clicked.connect(lambda: self.threadpool.start(self.startSim_worker))
        self.stopButton.clicked.connect(self.stop_calculation)

        self.defAction.triggered.connect(self.ask_set_defaults)


    def layout(self):

        


        leftvbox = QVBoxLayout()
        leftvbox.addWidget(self.params)
        leftvbox.addWidget(self.sweep)

        

        hbox = QHBoxLayout()
        hbox.addWidget(self.startButton)
        hbox.addWidget(self.stopButton)        
        leftvbox.addLayout(hbox)

        

        rightvbox = QVBoxLayout()

        tabs = QTabWidget()
        

        tabs.addTab(self.collisions, "Set Collisions")
        tabs.addTab(self.viewData, "View Data")
        tabs.addTab(self.args, "Command")

        rightvbox.addWidget(tabs)
        rightvbox.addWidget(self.console)

        hbox = QHBoxLayout()
        hbox.addLayout(leftvbox)
        hbox.addLayout(rightvbox)



        overall_vbox = QVBoxLayout()
        overall_vbox.addWidget(self.menuBar)
        overall_vbox.addLayout(hbox)

        self.setLayout(overall_vbox)
        pass

    def handle_sweep_choice(self):

        # need to clear the whole sweepwidget when this option changes
        # keep people from accidentally running over 100 terms
        self.sweep.clear_sweepoptions()



        self.sweep.variable_choice.currentTextChanged.connect(self.handle_sweep_choice)        
        var = self.sweep.get_var_option()
        self.params.environSection.EN_Td.setEnabled(True)
        self.params.numericSection.points.setEnabled(True)
        self.params.modelSection.terms.setEnabled(True)
        self.params.environSection.temperature.setEnabled(True)
        self.params.environSection.pressure.setEnabled(True)

        if var == "Field Strength":
            w = self.params.environSection.EN_Td
        elif var == "Grid Points":
            w = self.params.numericSection.points
        elif var == "Terms":
            w = self.params.modelSection.terms
        elif var == "Temperature":
            w = self.params.environSection.temperature
        elif var == "Pressure":
            w = self.params.environSection.pressure
        else:
            self.console.txt.append("Unexpected sweep-choice!")
            return

        w.setEnabled(False)
        pass

    def set_defaults(self):
        self.params.set_defaults()
        self.collisions.set_defaults()
        self.sweep.set_defaults()
        self.viewData.set_defaults()

    def ask_set_defaults(self):
        qm = QMessageBox()
        ret = qm.question(self, '', "Reset all settings to default?", qm.Yes | qm.No)

        if ret == qm.No:
            return

        self.set_defaults()


    def get_args(self):
        line = ""
        line = line + self.params.get_args()

        #print("first set gives:")
        #print(line)

        line = line + self.collisions.get_args()

        #print("second set gives:")
        #print(line)

        line = line + self.sweep.get_args()

        #print("third set gives:")
        #print(line)

        line = line.strip()
        splits = line.split(" ")
        line = ""
        for s in splits:
            line = line + s + " "

        return line

    # refresh the thread worker, essentially.
    # this is neccessary to run more than one sim
    def thread_cleanup(self):
        self.startSim_worker = Worker(self.attempt_calculation)
        #self.console.write("Concluded.")
        


    def attempt_calculation(self):
        # make sure .exe exists first
        if not os.path.exists(c.EXE_PATH):
            self.console.txt.append("Failed to start: exe not found in bin path")
            self.thread_cleanup()
            return
        
        #todo: safe validation of argstring

        # new: write args to screen every time calc is called
        # -- nope, turns out that breaks everything.
        #self.args.txt.setText(self.get_args())


        self.start_calculation()
        
        pass

    def start_calculation(self):

        self.console.clearButton.click()

        argstring = self.get_args()

        

        # do the filename check here
        if self.params.exportSection.export_name.text().strip() != "":

            try:
                with open(self.params.exportSection.export_name.text(), 'w'):  # if is valid name, this will not trip
                    pass
                os.remove(self.params.exportSection.export_name.text())

            except:
                self.console.txt.append("Cannot start calculation. Invalid directory name in 'export_name'.")
                self.thread_cleanup()
                return



        if argstring != None: # if argstring is valid

            self.startButton.setEnabled(False)
            self.stopButton.setEnabled(True)

            process = subprocess.Popen(c.EXE_PATH + " " + argstring, stdout=subprocess.PIPE)

            jobs.append(process)

            while process.poll() == None: # while the process is alive
                out = process.stdout.readline() # read its output to the console
                if out == '' and process.poll() != None:
                    break
                if out != '':
                    self.console.emitConsole.emit(out.decode('utf-8').strip())



            self.stopButton.setEnabled(False)
            self.startButton.setEnabled(True)

            #self.console.txt.ensureCursorVisible()
            #self.console.txt.verticalScrollBar().setValue(self.console.txt.verticalScrollBar().maximum())

            # clean up the final state of the console text
            # definitely suboptimal but shouldn't matter too much
            mytext = self.console.txt.toPlainText()
            self.console.clearButton.click()
            self.console.emitConsole.emit(mytext.strip())

            self.thread_cleanup()

        
        return


    def stop_calculation(self):
        if len(jobs) > 0:
            jobs[-1].kill()

        self.thread_cleanup()

        self.stopButton.setEnabled(False)
        self.startButton.setEnabled(True)

        
        #self.console.txt.verticalScrollBar().setValue(self.console.txt.verticalScrollBar().maximum())
        self.console.txt.setText(self.console.txt.toPlainText().strip())
        self.console.txt.ensureCursorVisible()
        self.console.txt.verticalScrollBar().setValue(self.console.txt.verticalScrollBar().maximum())
        
        return
        


# Worker placeholder for threading
class Worker(QRunnable):
    def __init__(self, fn):
        super(Worker, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
    @pyqtSlot()
    def run(self):
        #Initialise the runner function with passed args, kwargs.
        self.fn()


def closeEvent():
    if len(jobs) > 0:
        jobs[-1].kill() # safely kill the exe process so someone can exit out completely
    return


if __name__ == "__main__":

    import sys

    freeze_support()

    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    app.setStyleSheet(c.stylesheet_str+" QWidget{font-size:16px}")

    
    
    w = GUI()

    #w.show()

    w.startButton.setMinimumHeight(w.startButton.height() * 2)
    w.startButton.setMinimumWidth(int(QPushButton(w.startButton.text()).width() / 8))

    w.stopButton.setMinimumHeight(w.stopButton.height() * 2)
    w.stopButton.setMinimumWidth(int(QPushButton(w.startButton.text()).width() / 8))

    w.setMinimumSize(w.size())

    w.move(10, 10)

    #p = QPalette()
    #gradient = QLinearGradient(0, 0, 0, 400)
    #gradient.setColorAt(0.0, QColor(0, 0, 0))
    #gradient.setColorAt(1.0, QColor(255, 0, 0))

    #p.setBrush(QPalette.Window, QBrush(gradient))
    #w.setPalette(p)

    

    #window.setCentralWidget(w)
    #app.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowMinimizeButtonHint)

    sys.exit(app.exec_())