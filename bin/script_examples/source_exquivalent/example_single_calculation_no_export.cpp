// MultiBolt v3.1.1
// Example script: read LXCat cross section files for collisions and
// and set source to run a single calculation, then print the output to stdout
// without exporting data.

#include "multibolt"
#include <vector>


int main() {

	// stdout mode
	mb::mbSpeaker.printmode_normal_statements();
	// mb::mbSpeaker.printmode_debug_statements();
	// mb::mbSpeaker.printmode_silent();


	/* Collision Library Definition */

	// The Xsec files that have the collisions we are interested in
	std::vector<std::string> Xsec_fids = { "../../../../../../cross-sections/Biagi_N2.txt",
		"../../../../../../cross-sections/Biagi_Ar.txt" };

	// Only interested in collisions where the reactant are the neutrals in this example
	std::vector<std::string> names = { "N2", "Ar" }; 

	// Species prevalence of the two neutrals  - choosing half and half here for example
	std::vector<double> fracs = { 0.5, 0.5 };

	// Begin with an empty library
	lib::Library lib = lib::Library();

	lib.add_xsecs_from_LXCat_files(Xsec_fids);	// add all collisions from all files
	lib.keep_only(names);						// erase all collisions which do not belong to the species we are interested in
	lib.assign_fracs_by_names(names, fracs);	// declare the fraction of the species


	/* BoltzmannParameters Definition */

	mb::BoltzmannParameters p = mb::BoltzmannParameters();

	p.model = mb::ModelCode::HDGE;
	p.N_terms = 6;

	p.Nu = 200;

	p.iter_max = 100;	// Force break if you haven't converged within this many iterations to avoid wasting time
	p.iter_min = 4;		// Force continue if below this number of iterations
	p.weight_f0 = 1.0;	// Iteration weight. N2 and Ar behave well, 1.0 will suffice.

	p.conv_err = 1e-6;	// relative error


	p.USE_ENERGY_REMAP = true;	// Look for a new grid if necessary
	p.remap_grid_trial_max = 10;				// try a new grid no more than this many times
	p.remap_target_order_span = 10.0;			// how many orders would you prefer your EEDF to span (in eV^-3/2)


	/* grid max start: use one or the other*/

	/*p.USE_EV_MAX_GUESS = false;
	p.USE_EV_MAX_GUESS = false;*/
	
	 p.initial_eV_max = 100; 

	 p.EN_Td = 100;
	 p.p_Torr = 760;
	 p.T_K = 300;


	 /* Perform calculation */

	 mb::BoltzmannSolver solver = mb::BoltzmannSolver(p, lib);	// perform the actual calculation
	 mb::BoltzmannOutput out = solver.get_output();			// get output struct

	 out.print(); // Conclude by printing output data

	return 0;
}