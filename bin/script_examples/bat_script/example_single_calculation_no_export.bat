:: MultiBolt v3.1.1
:: See same-named cpp file for details

cd ../../../bin

multibolt_win64.exe ^
--model HD+GE ^
--N_terms 6 ^
--p_Torr 760 ^
--T_K 300 ^
--EN_Td 100 ^
--Nu 200 ^
--initial_eV_max 100 ^
--USE_ENERGY_REMAP ^
--conv_err 1e-6 ^
--weight_f0 1.0 ^
--iter_max 100 ^
--iter_min 4 ^
--remap_target_order_span 10 ^
--remap_grid_trial_max 10 ^
--export_location "../Exported_MultiBolt_Data/" ^
--export_name "example_single_calculation_no_export/" ^
--LXCat_Xsec_fid "../cross-sections/Biagi_N2.txt" ^
--LXCat_Xsec_fid "../cross-sections/Biagi_Ar.txt" ^
--species "N2" 0.5 ^
--species "Ar" 0.5 ^
--sweep_option EN_Td ^
--sweep_style def 100 ^
--NO_EXPORT

pause