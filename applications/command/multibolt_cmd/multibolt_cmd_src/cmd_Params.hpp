// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt_cmd"


// this is just a helper to do command line argument stuff
class SweepParams {

public:

	double start = arma::datum::nan;
	double stop = arma::datum::nan;
	int points = int(arma::datum::nan);

	mb::sweep_option sweep_option = mb::sweep_option::EN_Td;
	mb::sweep_style sweep_style = mb::sweep_style::lin;

	std::vector<double> defval = {};

};

// helper object for executable
class LibraryParams {
public:

	std::vector<std::string> LXCat_Xsec_fids = {};


	std::vector<std::string> species = {};  // goes together
	std::vector<double> fracs = {};    // goes together


	std::vector<std::string> processes = {};  // goes together
	std::vector<double> scales = {};    // goes together

	bool KEEP_ALL_SPECIES = false;

	lib::Scattering elastic_scattering = lib::isotropic_scattering();
	lib::Scattering excitation_scattering = lib::isotropic_scattering();
	lib::Scattering ionization_scattering = lib::isotropic_scattering();
	lib::Scattering superelastic_scattering = lib::isotropic_scattering();

};


// This is just a helper for accepting command line arguments
class ExportParams {
public:

	std::string export_location = mb::UNALLOCATED_STRING;
	std::string export_name = mb::UNALLOCATED_STRING;
	bool NO_EXPORT = false;

	bool EXPORT_XSECS = false; // tends to make large files, disable by default

	bool LIMIT_EXPORT = false; // 04/27/2022 - flag to only output the 4 major coefficients (k_iz_eff_N, W_BULK, DLN_BULK, alpha_eff_N)
	// found to be necessary when running dristributed cases on clusters, where literal number of files being written can be a limitation.
};