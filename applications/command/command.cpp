// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


// This source file, when compiled, makes the executable which accepts command-line arguments.

#include <multibolt_cmd> 
#include <multibolt>

//#include <tuple>

// Given the user-options, dev up a copy of the appropriate library (grid unfilled)
lib::Library dev_library(mb_cmd::LibraryParams lib_params, mb_cmd::SweepParams sweep_params, double sweep_val) {

	lib::Library thislib = lib::Library();
	thislib.add_xsecs_from_LXCat_files(lib_params.LXCat_Xsec_fids);
	thislib.assign_fracs_by_names(lib_params.species, lib_params.fracs);


	if (lib_params.KEEP_ALL_SPECIES == false) { // remove all species which were not explicitly asked for
		// important: re-order the library in the order of the asked-for species!
		thislib.keep_only(lib_params.species);
	}

	// insurance: in case of bin-frac, only keep the first two species
	if (sweep_params.sweep_option == mb::sweep_option::bin_frac) {
		thislib.keep_only({ thislib.allspecies[0]->name(), thislib.allspecies[1]->name() });
		mb::normal_statement("Notice: keeping only " + thislib.allspecies[0]->name() + " and " + thislib.allspecies[1]->name() + " for binary-fraction sweep.");
	}

	// Scale any in library as asked for
	for (auto it = lib_params.processes.begin(); it != lib_params.processes.end(); ++it) {

		auto this_i = it - lib_params.processes.begin();
		for (auto& spec : thislib.allspecies) {
			for (auto& x : spec->allcollisions) {

				if (mb::same_string(x->process(), *it)) {
					x->scale(lib_params.scales.at(this_i));
				}
			}
		}
	}


	double* it = &sweep_val;

	// affect p based on sweep
	switch (sweep_params.sweep_option) {
		case mb::sweep_option::bin_frac:

			// the second species will be assigned the opposite of the bin-frac
			thislib.assign_fracs_by_names({ thislib.allspecies[0]->name(), thislib.allspecies[1]->name() }, { *it, 1.0 - *it });

			break;
	}


	// 02/24/2023
	// assign scattering based on options
	for (auto& spec : thislib.allspecies) {

		for (auto& x : spec->allcollisions) {

			switch (x->code()) {
			case  lib::CollisionCode::elastic: x->scattering(lib_params.elastic_scattering);  break;
			case  lib::CollisionCode::effective : x->scattering(lib_params.elastic_scattering);  break;

			case  lib::CollisionCode::excitation : x->scattering(lib_params.excitation_scattering);  break;
			case  lib::CollisionCode::rotational: x->scattering(lib_params.excitation_scattering);  break;
			case  lib::CollisionCode::vibrational: x->scattering(lib_params.excitation_scattering);  break;

			case  lib::CollisionCode::superelastic : x->scattering(lib_params.superelastic_scattering);  break;

			case  lib::CollisionCode::ionization :  x->scattering(lib_params.ionization_scattering);  break;

			
			}
		}
	}
	



	return thislib;
}



int main(const int argc, const char** argv) {



	lib::libSpeaker.printmode_normal_statements();
	mb::mbSpeaker.printmode_normal_statements();

	

	mb::BoltzmannParameters basic_params;
	mb_cmd::SweepParams sweep_params;
	mb_cmd::ExportParams export_params;
	mb_cmd::LibraryParams lib_params;


	int multibolt_num_threads = 1;
    #ifdef MULTIBOLT_USING_OPENMP
		multibolt_num_threads = omp_get_num_threads();
	#endif

    
        
	

	bool DEFAULT_RUN = false;

	std::vector<std::string> argvec;

	if (argc <= 1) {
		mb::normal_statement("Zero arguments passed. Performing default MultiBolt run.");

        
        // bandage for incredibly annoying difference between Windows vis-studio and linux development
        // based on paths, cwd
        namespace fs = std::filesystem;
        
        std::string Biagi_N2_filename = "Biagi_N2.txt";
        std::string actual_xsec_path = "";
        if (fs::exists(Biagi_N2_filename)){
            actual_xsec_path = "./" + Biagi_N2_filename;
        }
        else if(fs::exists("./cross-sections/" + Biagi_N2_filename)){
            actual_xsec_path = "./cross-sections/" + Biagi_N2_filename;
        }
        else if(fs::exists("./../cross-sections/" + Biagi_N2_filename)){
            actual_xsec_path = "./../cross-sections/" + Biagi_N2_filename;
        }
        else if(fs::exists("./../../cross-sections/" + Biagi_N2_filename)){
            actual_xsec_path = "./../../cross-sections/" + Biagi_N2_filename;
        }
        else{
            actual_xsec_path = "./../../../cross-sections/" + Biagi_N2_filename; // last-ditch effort
        }
		

		// Equivalent default args

		argvec = std::vector<std::string>{

			"--model", "HD+GE",
			"--N_terms", "6",
			"--Nu", "200",
			"--p_Torr", "760",
			"--T_K", "300",

			"--sweep_option", "EN_Td",
			"--sweep_style", "lin", "100", "500", "5",

			//"--EN_Td", "100", // not necessary to set or pass arg if this is the sweep

			//"--NO_EXPORT",
			"--export_location", "../Exported_MultiBolt_Data",
			"--export_name", "Default",

			"--LXCat_Xsec_fid", "\'" + actual_xsec_path + "\'", // intended to run off a very specific path

			"--species", "N2", "1.0", // not electing to keep all species for space

			"--conv_err", "1e-4",			/* optional param */
			"--iter_max", "100",			/* optional param */
			"--iter_min", "4",				/* optional param */
			"--initial_eV_max", "100",		/* optional param */
			"--weight_f0", "1.0",			/* optional param */

			"--USE_EV_MAX_GUESS",			/* optional param */
			"--USE_ENERGY_REMAP",				/* optional param */
			"--remap_target_order_span", "10",	/* optional param */
			"--remap_allowance", "5",			/* optional param */

			//"--USE_NU_REMAP",				    /* optional param */
			//"--remap_Nu_max","2000",					/* optional param */
			//"--remap_Nu_increment","200",					/* optional param */

			"--remap_grid_trial_max", "10",		/* optional param */

			"--elastic_scattering", "Isotropic",
			"--excitation_scattering", "Isotropic",
			"--ionization_scattering", "Isotropic",
			"--superelastic_scattering", "Isotropic",

			"--sharing", "0.5",

			"--EXPORT_XSECS",

			"--KEEP_ALL_SPECIES",

			//"--LIMIT_EXPORT",git 

			//"--SILENT",

			"--multibolt_num_threads", "max",

			//"--help"

			//"--SHY"
			//"--DONT_ENFORCE_SUM"

			"--interp_method", "Linear"
			
		};

		DEFAULT_RUN = true;
		mb::mbSpeaker.printmode_normal_statements();


	}
	else {
		// always skip first arg because it is just the program name and path
		for (int i = 1; i < argc; ++i) {

			std::string s = argv[i];

			argvec.push_back(s);
		}
	}

	try {

		// enforce that argvec is usable by removing any quote-mark wrapping
		for (int i = 0; i < argvec.size(); ++i) {

			std::string s = argvec.at(i);

			auto N = s.length();

			char first = s[0];
			char last = s[N - 1];

			// if is wrapped in double quotes
			if ((first == '"' && last == '"') || (first == '\'' && last == '\'')) {
				s = s.substr(1, N - 2);
				N = N - 1;
			}

			argvec.at(i) = s;
		}


		auto tup = mb_cmd::parse_args(argc, argvec); // transforms par into a usable state

		basic_params = std::get<0>(tup);
		lib_params = std::get<1>(tup);
		sweep_params = std::get<2>(tup);
		export_params = std::get<3>(tup);
		multibolt_num_threads = std::get<4>(tup);

	}
	catch (...) {
		mb::error_statement("Bad command line arguments. Try --help for details.");
		::exit(-1);
	}

	using std::cout;
	using std::endl;


	mb::normal_statement("*** ");
	mb::normal_statement("*** MULTIBOLT");
	mb::normal_statement("*** A multi-term Boltzmann equation solver");
	mb::normal_statement("*** v3.1.4");
	mb::normal_statement("*** Most recent development: 10/2024");
	mb::normal_statement("*** ");


	// stop a calculation here if the user is trying to write to folders that cannot exist (invalid chars, reserved names, etc)
	if (!export_params.NO_EXPORT) { // dont care about this check if suppressed

		bool valid_export_location = mb::is_valid_dirstring(export_params.export_location);
		bool valid_export_name = mb::is_valid_dirstring(export_params.export_name);

		if (!valid_export_location) {
			mb::error_statement("Export location argument '" + export_params.export_location + "' is not a path which can be written to on this machine.");
		}
		if (!valid_export_name) {
			mb::error_statement("Export name argument '" + export_params.export_name + "' is not a string which can be a valid directory on this machine.");

		}

		if (!valid_export_name || !valid_export_location) {
			mb::error_statement("Bad command line arguments. Try --help for details.");
			::exit(-1);
		}
	}
	


	

    #ifdef MULTIBOLT_USING_OPENMP
        if (multibolt_num_threads > omp_get_max_threads()) {
            mb::normal_statement("Notice: options asked for more threads than are available. Thread number will be truncated.");
            multibolt_num_threads = std::min(multibolt_num_threads, omp_get_max_threads());
        }
        mb::normal_statement("MultiBolt is using up to " + std::to_string(multibolt_num_threads) + " thread(s).");
    #endif
    
    #if !defined(ARMA_NO_DEBUG)
        mb::normal_statement("Notice: MultiBolt was compiled without the ARMA_NO_DEBUG directive. This may introduce major performance losses.");
    #endif


	// Develop sweep vector values
	arma::vec sweep;

	switch (sweep_params.sweep_style) {
	case mb::sweep_style::lin:
		sweep = arma::linspace(sweep_params.start, sweep_params.stop, sweep_params.points);
		break;
	case mb::sweep_style::log:
		sweep = arma::logspace(sweep_params.start, sweep_params.stop, sweep_params.points);
		break;
	case mb::sweep_style::reg:
		sweep = arma::regspace(sweep_params.start, sweep_params.points, sweep_params.stop); // points is actually delta here
		break;
	case mb::sweep_style::single:
		sweep = arma::vec(1, arma::fill::zeros);
		break;
	case mb::sweep_style::def:
		sweep = sweep_params.defval;
		break;
	}


	

	// vector of outputs allotted
	std::vector<mb::BoltzmannOutput> outputs = std::vector<mb::BoltzmannOutput>(sweep.size()); // push-back for every go
	std::vector<mb::BoltzmannParameters> parameters = std::vector<mb::BoltzmannParameters>(sweep.size()); // push-back for every go

	#ifdef MULTIBOLT_USING_OPENMP
		omp_set_nested(1);
	#endif

	//slightly dumb repetitive work-around, but it works

#ifdef MULTIBOLT_USING_OPENMP
	// placement of that preproc def is critical, can't just wrap it in ifdef

	#pragma omp parallel num_threads(multibolt_num_threads)
	#pragma omp for
	for (int i = 0; i < sweep.size(); ++i) {
		

		// this iters specific params
		mb::BoltzmannParameters p = basic_params; // copy

		// affect p based on sweep
		double* it = &sweep(i);
		switch (sweep_params.sweep_option) {
			case mb::sweep_option::none:
				break;
			case mb::sweep_option::EN_Td:
				p.EN_Td = *it;
				break;
			case mb::sweep_option::T_K:
				p.T_K = *it;
				break;
			case mb::sweep_option::Nu:
				p.Nu = int(*it);
				if (p.USE_NU_REMAP == true) {
					p.USE_NU_REMAP = false;
					mb::normal_statement("Notice: USE_REMAP_NU is disabled while Nu is the sweep variable.");
				}
				break;
			case mb::sweep_option::N_terms:
				p.N_terms = int(*it);
				break;
			case mb::sweep_option::p_Torr:
				p.p_Torr = *it;
				break;
			break;
		}


		// Develop library
		lib::Library lib = dev_library(lib_params, sweep_params, *it);

		mb::BoltzmannSolver run = mb::BoltzmannSolver(p, lib);
		mb::BoltzmannOutput out = run.get_output();

		outputs.at(i) = run.get_output(); // record
		parameters.at(i) = p; // record


		if (DEFAULT_RUN) {
			mb::normal_statement("Default calculation concludes with the following output:\n");
			out.print();
		}
	}

#else
		// code block which runs **without** open-mp

		for (int i = 0; i < sweep.size(); ++i) {


			// this iters specific params
			mb::BoltzmannParameters p = basic_params; // copy

			// affect p based on sweep
			double* it = &sweep(i);
			switch (sweep_params.sweep_option) {
			case mb::sweep_option::none:
				break;
			case mb::sweep_option::EN_Td:
				p.EN_Td = *it;
				break;
			case mb::sweep_option::T_K:
				p.T_K = *it;
				break;
			case mb::sweep_option::Nu:
				p.Nu = int(*it);
				if (p.USE_NU_REMAP == true) {
					p.USE_NU_REMAP = false;
					mb::normal_statement("Notice: USE_NU_REMAP is disabled while Nu is the sweep variable.");
				}
				break;
			case mb::sweep_option::N_terms:
				p.N_terms = int(*it);
				break;
			case mb::sweep_option::p_Torr:
				p.p_Torr = *it;
				break;
				break;
			}


			// Develop library
			lib::Library lib = dev_library(lib_params, sweep_params, *it);


			mb::BoltzmannSolver run = mb::BoltzmannSolver(p, lib);
			mb::BoltzmannOutput out = run.get_output();

			outputs.at(i) = run.get_output(); // record
			parameters.at(i) = p; // record


			if (DEFAULT_RUN) {
				mb::normal_statement("Default calculation concludes with the following output:\n");
				out.print();
			}
	}

#endif


	// Initialize exporter
	mb::Exporter exp = mb::Exporter();
	// Now that all outputs have been collected,
	// Write them in a loop
	if (export_params.NO_EXPORT == false) {

		
		int icount = 0;
		for (double* it = sweep.begin(); it != sweep.end(); it++) {
			// Re-Develop library for the purpose of writing in the export
			lib::Library basic_lib = dev_library(lib_params, sweep_params, *it);

			if (it == sweep.begin()) {
				exp = mb::Exporter(basic_lib, basic_params, outputs.at(icount),
					export_params.export_location, export_params.export_name, sweep_params.sweep_option, export_params.EXPORT_XSECS, export_params.LIMIT_EXPORT);
			}

			exp.write_this_run(parameters.at(icount), outputs.at(icount));

			icount++;
		}

	}

	mb::debug_statement("Leaving source...");

	return 0;
}






