cmake_minimum_required(VERSION ${CMAKE_MINIMUM_REQUIRED_VERSION})


get_filename_component(ProjectId ${CMAKE_CURRENT_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
project(${ProjectId})


#include_directories(${ProjectId} ${__INCLUDE_DIRECTORIES})
add_executable(${ProjectId} ${ProjectId}.cpp)
#target_link_libraries(${ProjectId} ${__LINK_LIBRARIES})

message("")
message(STATUS "*** Checking <filesystem> compilation.")
try_compile(
  RESULT_VAR
    ${CMAKE_CURRENT_BINARY_DIR}
  SOURCES
    ${CMAKE_CURRENT_SOURCE_DIR}/${ProjectId}.cpp
)
message(STATUS "*** <filesystem> compilation confirmed.")

try_run(FILESYSTEM_OUTPUT_VARIABLE
  RESULT_VAR
    ${CMAKE_CURRENT_BINARY_DIR}
  SOURCES
    ${CMAKE_CURRENT_SOURCE_DIR}/${ProjectId}.cpp
)

if(FILESYSTEM_OUTPUT_VARIABLE EQUAL 0)

    message(STATUS "*** <filesystem> runtime confirmed.")

else()

    message(FATAL_ERROR "*** <filesystem> runtime fails. This tends to be a compiler-version issue.
                        *** Current known fixes:
                        ***     g++-9 (8.2 and lower is known to fail)")
    
endif()
