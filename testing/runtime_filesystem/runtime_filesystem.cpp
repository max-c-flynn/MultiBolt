#include <iostream>
#include <filesystem> // IF THIS LINE FAILS COMPILATION ON LINUX, YOU MUST USE CXX COMPILER g++-9 OR HIGHER (g++8.2? is known to cause problems)
#include <string>

int main(){
    
    namespace fs = std::filesystem;

    //std::cout << "MultiBolt test: filesystem" << std::endl;
    //std::cout << "\tstd::filesystem is brittle with certain versions of g++." << std::endl;
    //std::cout << "\tIf the following seg-faults, use g++ 9 or higher." << std::endl;
    
    
    fs::path(".");
    fs::directory_iterator(".");
    
    std::cout << "MultiBolt::testing::filesystem:: SUCCESS" << std::endl;
    
    return 0;
}
