// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


// This is a source file which essentially only #includes <multibolt>
// and is basically a minimal-compilation test

#include <multibolt>


int main() {

    std::cout << "MultiBolt minimal file: Hello world!" << std::endl;
    
	return 0;
}






