// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once





#ifndef MULTIBOLT_CONFIG
#define MULTIBOLT_CONFIG



// Preprocessor definitions specific to MultiBolt always lead with MULTIBOLT_
// other definitions come from armadillo or otherwise.
// I do not reccomend adjusting the Armadillo related ones unless you know what you are doing
// ----




// some arma/superlu notes:
//
// 1)	Be aware that if armadillo was #include'd by ANYTHING before MultiBolt, 
//		these preprocessor defs will not do anything to the already-included headers
//		Either #include <multibolt> first, or copy these configs to the top of your own source (above all other #includes)
//
// 2)	Expect to link, at compile-time, to a static superlu binary (compiled) object
//		For example, 'superlu_win64.lib' (may be .a instead on Linux)
//		This is machine specific! Compile your own if necessary.
//		Use pre-compiled objects AT YOUR OWN RISK.


//// 01/18/2022
//// Bandage:
//		Older dev versions of MultiBolt got away with linking a chunky 600 MB
//		Version of SuperLU compiled using Intel MKL libraries
//		This ballooned the size of basically everything, but it did compile without complaint
//		Have moved to compiling against OpenBLAS instead. Dramatically shrinks all compiled file sizes.
//		The issue: compiling OpenBLAS to contain LAPACK routines is not straight-forward on Windows (sorry!).
//		The bandage: force armadillo to never talk about LAPACK (okay for MultiBolt)
#if !defined(ARMA_DONT_USE_LAPACK)
    #define ARMA_DONT_USE_LAPACK
#endif  
//// IMPORTANT: if you need armadillo to use lapack for your work,
//// compile your own libraries, link appropriately, and comment out the above



//// Do not comment-out unless SuperLU dependency has been removed.
//// Armadillo needs to be told it is allowed to use SuperLU when solving sparse matrices.
#if !defined(ARMA_USE_SUPERLU)
    #define ARMA_USE_SUPERLU
#endif  


//// Commenting introduces major performance loss due to Armadillo bounds checking.
//// Uncomment for release and performance tests.
#if NDEBUG
	#if !defined(ARMA_NO_DEBUG)
		#define ARMA_NO_DEBUG
	#endif
#endif



// New as of 03/24/2022
#ifdef _OPENMP    // this is a flag normally passed in by VS, cmake, etc
	// Comment the below line to totally-disable multibolt from using OpenMP regardless of its presense
	#define MULTIBOLT_USING_OPENMP	
#endif



// For visual studio
#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING



#endif

