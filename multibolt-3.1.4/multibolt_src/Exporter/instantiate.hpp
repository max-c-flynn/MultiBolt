// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"

// TODO: whole file probably belongs to Exporter

// Make all the .txt files which should exist from the start
// This will not make any f0 or f1 files because those exist per-run
void mb::Exporter::instantiate_toplevel_txts() {

	for (auto& exp : exportables) {
		
		mb::init_txt(exp.dir, exp.filename, exp.headerlines, exp.key1, exp.key2);
		mb::debug_statement("Made a toplevel txt: " + exp.key1 + " " + exp.key2);
	}

	return;
}

// this is used to make f0 or f1 files instead of the above
void mb::Exporter::instantiate_perRun_txts() {
	for (auto& exp : perRun_exportables) {
		mb::init_txt(exp.dir, exp.filename, exp.headerlines, exp.key1, exp.key2);
		mb::debug_statement("Made a perRun txt: " + exp.key1 + " " + exp.key2);
	}
	return;
}





// makes all the folders inside the dir, but none of the txts
void mb::Exporter::make_file_hierarchy() {

	using std::ifstream;
	using std::stringstream;
	namespace fs = std::filesystem;
	using namespace fs;


	if (this->export_limited == true) {
		return; // ignore all this in the limited case
	}

	// if you got this far, the internal structure of the folder does not yet exist, you don't need to check for pre-existence

	// If you want to add any new directories for any reason, add an element to the below
	std::vector<std::filesystem::path> dirs = { fs::path(full_path.string() + mb::sep() + "EEDFs_f0").make_preferred().lexically_normal(),
												fs::path(full_path.string() + mb::sep() + "EEDFs_f1").make_preferred().lexically_normal(),
												fs::path(full_path.string() + mb::sep() + "Total").make_preferred().lexically_normal(),
												fs::path(full_path.string() + mb::sep() + "PerGas").make_preferred().lexically_normal() };


	// Add in a folder per-gas
	for (int Ng = 0; Ng < lib.allspecies.size(); ++Ng) {
		dirs.push_back(fs::path(full_path.string() + mb::sep() + "PerGas" + mb::sep() + "Gas_" + std::to_string(Ng)).make_preferred().lexically_normal());
	}


	for (auto& dir : dirs) {
		fs::create_directory(dir);
	}

	return;
}