// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// A BoltzmannSolver is the physics owner of most of the math
// Construct one to perform a calculation
// When finished, call .get_output() to receive output object

class BoltzmannSolver {

	public:

		mb::BoltzmannParameters p;
		lib::Library lib; // <- from collisionlibrary
		mb::BoltzmannOutput out;

	private:

		bool solution_succesful = true; // right now, relevant to "SHY"
	
		double Np = arma::datum::nan; // gas environment particle density [m^-3]
		// TODO: if xsec scales become pointers, Np becomes a great way
		// to auto-scale density-dependent xsecs like 3-body attachment in O2

		double E0 = arma::datum::nan; // Electric field strength [V/m]

		mb::Grid g; // container for energy-grid objects


		// -- iteration counters
		int iter_f0_SST = 0;
		int iter_f0_HD = 0;
		int iter_f1L = 0;
		int iter_f1T = 0;
		int iter_f2L = 0;
		int iter_f2T = 0;


		// -- Eigenvalues of numerical schemes
		double alpha_eff_N = arma::datum::nan;	// of SST (primary Townsend coefficient)
		double omega0 = arma::datum::nan;		// of 0 (effective ionization rate coefficient)
		double omega1 = arma::datum::nan;		// of 1L (bulk drift velocity)
		double omega2 = arma::datum::nan;		// of 2L (component of longitudinal/transverse diffusion coefficients)
		double omega2_bar = arma::datum::nan;	// of 2T (component of longitudinal/transverse diffusion coefficients)


		// -- Distribution function solutions
		arma::colvec x_f;
		arma::colvec x_f1L;
		arma::colvec x_f1T;
		arma::colvec x_f2L;
		arma::colvec x_f2T;

		// -- Scattering matrix - only need to calculate once per grid.
		// some conditions (many collisions, anisotropic scattering) make the filling of this mat take longer than otherwise
		arma::SpMat<double> A_scattering;

		// -- wall_clock, replacement for MATLABs tic/toc
		arma::wall_clock timer;
		double calculation_time = arma::datum::nan;

		// -- special flags
		bool COLD_CASE = false; // flips if p.T_K is found to be zero, disables thermal contribution
		bool NO_ITERATION = false; // flips if the grid is found to contain no ionization or attachment, ie solutions need only 1 iteration
		bool ZERO_FIELD_CASE = false; // flips if E/N = 0

		
		// -- some condition trackers
		double	present_eV_max = arma::datum::nan;
		double	present_weight_f0 = arma::datum::nan;
		int		present_Nu = arma::datum::nan;					// 08/2023

	
	public:

		// -- Constructor is the actual execution as long as parameters were actually passed in

		//default, do nothing
		BoltzmannSolver() {};

		
		// -- On construction, perform a MultiBolt calculation
		BoltzmannSolver(const mb::BoltzmannParameters p, const lib::Library lib) {

			timer.tic();

			this->p = p;
			this->lib = lib;
			this->present_eV_max = p.initial_eV_max; // may change during calc due to grid_remap
			this->present_weight_f0 = p.weight_f0; // may change during calc due to stability
			this->present_Nu = p.Nu; // may change during calc due to Nu_remap

			mb::normal_statement("Beginning MultiBolt calculation...");
			mb::debug_statement("With parameters:");
			if (mb::mbSpeaker.get_outOp() == mb::OutputOption::DEBUG) {
				this->p.print();
			}

			check_validity();

			if (p.USE_EV_MAX_GUESS == true) {
				this->present_eV_max = this->guess_eV_max();
				this->p.initial_eV_max = present_eV_max;
				mb::normal_statement("Grid Guess: using " + mb::mb_format(present_eV_max) + " eV.");
			}

			if (p.EN_Td == 0) {
				mb::debug_statement("Notice: field strength is zero.");
				this->ZERO_FIELD_CASE = true;
			}

			if (p.T_K == 0) {
				mb::debug_statement("Notice: gas temperature is zero, using cold-case.");
				this->COLD_CASE = true;
				this->Np = 101325.0 / mb::KB / (300.0) * (760.0 / p.p_Torr); // just to use a real finite value
			}
			else {
				this->Np = 101325.0 / 1.38E-23 / p.T_K * (760.0 / p.p_Torr);
			}

			if (p.Nu > p.remap_Nu_max) {
				mb::normal_statement("Notice: user-input grid resolution Nu is already large, Nu remap will not occur.");
			}


			this->E0 = this->Np * p.EN_Td * 1E-21; // electric field in V/m
				
			// -- Done with setup, move to calculation
			execute();


			calculation_time = timer.toc();
			timer = arma::wall_clock(); // Turn off the timer by blipping it

			// Now that we are finished, shovel in calculations to output
			this->out = mb::BoltzmannOutput();
			carve_output();
			fill_output();

			mb::normal_statement("Concluded a MultiBolt calculation.");
			
			if (mb::mbSpeaker.get_outOp() == mb::OutputOption::DEBUG) {
				mb::debug_statement("With output:");
				this->out.print();
			}

			return;

		}


	private:

		void execute();

		void check_validity();

		void carve_output(); // output struct instantiation, sizing
		void fill_output(); // output struct filling

		void solve_f0_SST();
		void gov_eq_f0_SST(arma::sword ell, arma::SpMat<double>& A);

		void solve_f0_HD();
		void gov_eq_f0_HD(arma::sword ell, arma::SpMat<double>& A);

		void solve_f1L();
		void gov_eq_f1L(arma::sword ell, arma::SpMat<double>& A, arma::Col<double>& b);

		void solve_f1T();
		void gov_eq_f1T(arma::sword ell, arma::SpMat<double>& A, arma::Col<double>& b);

		void solve_f2L();
		void gov_eq_f2L(arma::sword ell, arma::SpMat<double>& A, arma::colvec& b);

		void solve_f2T();
		void gov_eq_f2T(arma::sword ell, arma::SpMat<double>& A, arma::colvec& b);


		// -- Collision operators
		void C_ela(arma::sword ell, arma::SpMat<double>& A);
		void C_exc(arma::sword ell, arma::SpMat<double>& A);
		void C_att(arma::sword ell, arma::SpMat<double>& A);
		void C_iz(arma::sword ell, arma::SpMat<double>& A);
		void C_sup(arma::sword ell, arma::SpMat<double>& A);
		void full_collision_operator(arma::sword ell, arma::SpMat<double>& A);


		// R = C[f], as in, 
		// BE == Rin - Rout
		// maybe do something with this form later
		arma::colvec C_in_att(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_out_att(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_in_ela(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_out_ela(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_in_eff(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_out_eff(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_in_exc(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_out_exc(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_in_sup(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_out_sup(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_in_iz(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_out_iz(const arma::sword ell, const arma::colvec& sigma, const double frac);


		void boundary_conditions(arma::sword ell, arma::SpMat<double>& A, arma::colvec& b);
		
		
		void set_grid(); // Act on Grid container based on settings


		// -- Calculate-relevant functions

		arma::colvec calculate_s_Te_eff(); // total xsec weighted by gas-fractions

		double calculate_rate(const arma::uword i_x); // for any rate coefficient
		double calculate_rate(const std::shared_ptr<lib::AbstractXsec>& x);
		double calculate_total_rate(const arma::uvec i_x); // pass in g.i_elastic, g.i_excitation, etc 
		double calculate_total_S(const arma::colvec& f, const arma::uvec i_x); // more generalized form of the above, useful for higher order schemes

		double calculate_growth(const arma::uword i_x);
		double calculate_growth(const std::shared_ptr<lib::AbstractXsec>& x);
		double calculate_total_growth(const arma::uvec i_x);
		
		double calculate_k_iz_eff_N(); 

		double calculate_W_f0();
		double calculate_W_BULK();
		double calculate_energy_W_f0();		// 04/2023

		double calculate_muN_f0();
		double calculate_muN_BULK();
		double calculate_energy_muN_f0();	// 04/2023

		double calculate_avg_en();

		double calculate_alpha_eff_N();

		double calculate_DFTN();
		double calculate_DTN_BULK();

		double calculate_DFLN();
		double calculate_DLN_BULK();

		double calculate_DN_f0();
		double calculate_DN_f0_nu();
		double calculate_energy_DN_f0();	// 04/2023
		double calculate_energy_DN_f0_nu();	// 04/2023


		double calculate_DT_mu();
		double calculate_DL_mu();
		double calculate_D_mu();

		
		// total inelastic xsec for the species
		double species_total_inelastic(const double eV, const arma::uword i_spec);
		arma::colvec species_total_inelastic(const arma::colvec& eV, const arma::uword i_spec);


		// guess grid based on field or temp-driven conditions
		double guess_eV_max();


		mb::RemapChoice check_grid_max(); // checking related to energy remap
		void lower_grid();
		void raise_grid_max_little();
		void raise_grid_max_lots();
		void raise_grid_max_extrap();
			

		bool check_is_solution_failing(int level);

		double  Du_at_u(const double u);
		arma::colvec  Du_at_u(const arma::colvec& u);

		arma::uvec get_safe_eedf_ix();

		public:
			arma::colvec calculate_MaxwellBoltzmann_EEDF(); // a courtesy: the limit case of the EEDF with no electron-neutral collisions in hot temperature
			// above is not applicable for cold case
			// important: this is *not* the same thing as the EEDF at thermal equilibrium in any real gas 


			double check_f0_normalization();
		
	public:

		// -- Add getters as necessary for your needs

		mb::BoltzmannOutput get_output()
		{
			return out;
		}
		mb::BoltzmannParameters get_parameters() {
			return p;
		}

		arma::colvec get_x_f() { // useful for time-dependent solvers
			return x_f;
		}

		arma::sp_mat get_scattering_matrix() { // useful for time-dependent solvers ?
			return A_scattering;
		}
};


//// Boundary conditions
/// A note: schemes beyond _0 or SST are insensitive to boundary conditions
//// (odd BC is subtly incorporated in the discretization scheme and does not require an explicit fixed point)
void mb::BoltzmannSolver::boundary_conditions(arma::sword ell, arma::SpMat<double>& A, arma::colvec& b) {
	
	if (ell % 2 == 0) { //% ell = (even), setup even BC

		// f^ l(u = u_max) = 0; for l = (even)

		arma::sword ROW = ell * present_Nu + (present_Nu - 1);
		arma::sword COL = ROW;

		A.row(ROW).zeros();
		A(ROW, COL) = 1.0;
		b.at(ROW) = 0.0;
	}
	else { // if is odd instead // <- do not add in, unnecessary

	}

	return;
};


// Decision-maker while p.USE_EV_MAX_GUESS = true
// Empirical! based on personal-experience
// Distribution is either field-driven (~Druyvesteyn)
// or temperature-driven (~Maxwell-Boltzmann)
// For the constant-ela case, these can be solved exactly
double mb::BoltzmannSolver::guess_eV_max() {

	// Find useful shape of xsec for guessing Druyvesteyn-like field-driven swarm
	double AVG_CONSTANT_S = 0;
	double AVG_MRATIO = 0;


	arma::colvec rough_eV = arma::linspace(0, 10, 50);


	for (auto& spec : this->lib.allspecies) {
		for (auto& x : spec->ela) {

			arma::colvec s = x->eval_at_eV(rough_eV);
			arma::uvec ix = arma::find_finite(s);
			double this_s = arma::as_scalar(arma::mean(s(ix)));


			AVG_CONSTANT_S += spec->frac() * this_s / spec->n_ela(); 
			AVG_MRATIO += spec->frac() * x->Mratio() / spec->n_ela();
		}

		for (auto& x : spec->eff) {
			AVG_CONSTANT_S += spec->frac() * arma::as_scalar(arma::mean(x->eval_at_eV(rough_eV))) / spec->n_eff(); // dont care about inel contribution
			AVG_MRATIO += spec->frac() * x->Mratio() / spec->n_eff();
		}
	}

	// choose which physics is the driver based on which analytic swarm has the larger average energy


	// Druyvesteyn has an analytic shape and average energy, find analytically
	double GAMMA_5_4 = 0.906; // "gamma" function pre-evals
	double GAMMA_3_4 = 1.225; // "gamma" function pre-evals
	double arg_druy =  // arg of exponent in SI
		std::pow(2. / mb::ME, 2.) / (4. / 3. * 1. / AVG_MRATIO * std::pow(mb::QE / mb::ME / AVG_CONSTANT_S * p.EN_Td * 1e-21, 2.));
	double norm_druy = GAMMA_3_4 / (2. * std::pow(arg_druy, 3.0 / 4.0)); // norm factor in SI
	double Druy_avg_en = (GAMMA_5_4 / norm_druy / (2. * std::pow(arg_druy, 5. / 4.))) / mb::QE;
	
	double Druy_head = 1. / norm_druy; // f(0) equivalent  in SI
	double eV_max_Druy = std::sqrt(std::log(std::pow(10, - p.remap_target_order_span) ) * -1. / arg_druy) / mb::QE;
	



	// Can also do the same for a MB if T_K != 0
	double arg_MB = 1. / (p.T_K * mb::KB);
	double norm_MB = std::sqrt(mb::PI) / (2. * std::pow(arg_MB, 3. / 2.));
	double MB_head = 1. / norm_MB;

	double MB_avg_en = (3.0 / 2.0) * mb::KB * p.T_K / mb::QE;
	double eV_max_MB = std::log(  std::pow(10, -p.remap_target_order_span) ) / ( - arg_MB) / mb::QE;

	

	bool IS_FIELD_DRIVEN = false;
	if (p.T_K == 0) {
		IS_FIELD_DRIVEN = true;
	}
	else if (p.EN_Td == 0) {
		IS_FIELD_DRIVEN = false;
	}
	else {
		IS_FIELD_DRIVEN = Druy_avg_en > MB_avg_en;
	}

	

	double solution = 0;
	if (IS_FIELD_DRIVEN) {

		solution = sqrt(Druy_avg_en) + p.EN_Td; // attempts to use 'Druy' at low E/N but grade towards E/N for very large E/N
		
		// -- found another empirical treatment while experimenting with Argon. Not going to use in case its too gas-specific.
		//solution = 10. * std::pow(p.EN_Td, 1. / 3.);
		//if (solution < p.EN_Td) { solution = p.EN_Td; }

	}
	else {
		solution = eV_max_MB;
	}

	if (!std::isfinite(solution)) {
		mb::normal_statement("Initial grid guess returned non-finite solution. Decision falling back to 1, 'EN_Td', or 100. This is unexpected behavior: please double check your settings!");
		solution = std::min(p.EN_Td, 100.0);
		solution = std::max(p.EN_Td, 1.0);
	}

	return solution;
};


// The eedf as-calculated may be problematic near grid-ends depending on the fitness of the grid
// used to solve it. Many code sections need a chopped version of all positive descending values
// to help make gridding decisions.
arma::uvec mb::BoltzmannSolver::get_safe_eedf_ix() {

	arma::uword pt_head = round(present_Nu * 0.01);
	arma::uword pt_tail = round(present_Nu * 0.90);

	arma::uvec is_positive = arma::find(x_f(g.ell0_idx) > 0);
	arma::uvec in_range = arma::regspace<arma::uvec>(pt_head, pt_tail);

	arma::uvec safe_ix = arma::intersect(is_positive, in_range);

	return safe_ix;
}