// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// Excitation operator
// strong cooling effect, tends to insert electrons near the origin
void mb::BoltzmannSolver::C_exc(arma::sword ell, arma::SpMat<double>& A) {

	for (auto i_x : g.i_excitation) {

		auto xsec = g.xsec_ptr(i_x);
		auto spec = g.species_ptr(g.i_collision_species(i_x));

		double frac = g.i_frac(i_x);
		if (mb::doubles_are_same(frac, 0)) { continue; }

		mb::debug_statement("Applying C_exc[" + std::to_string(ell) + "] with process " + xsec->process() + " in species " + spec->name());

		for (arma::sword k = 0; k < present_Nu; ++k) {
			double u = 0;
			double uprimeprime = 0;
			double totalICS = 0;

			// electron scatters-out with energy u + uk
			arma::sword dk_exc;
			if (is_even_or_zero(ell)) {
				dk_exc = arma::sword(round((xsec->eV_thresh()) * mb::QE / Du_at_u(g.u_e(k)))); // grid-steps of exc
			}
			else {
				dk_exc = arma::sword(round((xsec->eV_thresh()) * mb::QE / Du_at_u(g.u_o(k)))); // grid-steps of exc
			}

			// electron scatters-in at u + ek
			if (k + dk_exc < present_Nu && k + dk_exc >= 0) {
				// f0(k + dk_exc)

				if (is_even_or_zero(ell)) {
					uprimeprime = g.u_e(k + dk_exc);

					totalICS = g.sigma_e(k + dk_exc, i_x);
				}
				else {
					uprimeprime = g.u_e(k + dk_exc);
					totalICS = g.sigma_o(k + dk_exc, i_x);
				}
				u = uprimeprime - xsec->eV_thresh() * mb::QE; // safer option if rotations exist

				double Phi = xsec->scattering()->scattering_integral(uprimeprime / mb::QE, u / mb::QE, ell);

				A.at((ell * present_Nu) + k, (ell * present_Nu) + k + dk_exc) +=
					+ frac * uprimeprime * Np * Phi * totalICS;
			}




			// electron scatters-out at energy u

			if (is_even_or_zero(ell)) {
				u = g.u_e(k);
				totalICS = g.sigma_e(k, i_x);
			}
			else {
				u = g.u_o(k);
				totalICS = g.sigma_o(k, i_x);
			}

			A.at((ell * present_Nu) + k, (ell * present_Nu) + k) +=
				-frac * u * Np * totalICS;

		}
	}


}
