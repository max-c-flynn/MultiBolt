// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// superelastic operator
// current implementation assumes isotropic scattering
void mb::BoltzmannSolver::C_sup(arma::sword ell, arma::SpMat<double>& A) {


	// superelastic processes have opposite sign convention to excitation
	// they heat the eedf rather than cool it

	for (auto i_x : g.i_superelastic) {

		auto xsec = g.xsec_ptr(i_x);
		auto spec = g.species_ptr(g.i_collision_species(i_x));

		double frac = g.i_frac(i_x);
		if (mb::doubles_are_same(frac, 0)) { continue; }

		mb::debug_statement("Applying C_sup[" + std::to_string(ell) + "] with process " + xsec->process() + " in species " + spec->name());

		// index to inject electron at
		//arma::sword dk_exc = arma::sword(round((xsec->parent_eV_thresh()) * mb::QE / g.Du)); // is actually retrieving the eV_thresh of the parent in this case



		for (arma::sword k = 0; k < present_Nu; ++k) { // row #

			double u = 0; // energy of electron post-collision
			double uprime = 0; // energy of electron pre-collision (Loureiro + Amorim notation for superelastics)

			double totalICS = 0;

			// steps in grid assosciated with ionization energy
			arma::sword dk_exc;
			if (is_even_or_zero(ell)) {
				dk_exc = arma::sword(round((xsec->parent_eV_thresh()) * mb::QE / Du_at_u(g.u_e(k))));
			}
			else {
				dk_exc = arma::sword(round((xsec->parent_eV_thresh()) * mb::QE / Du_at_u(g.u_o(k))));
			}
			
			// electron scatters-in at u - uk
			if (k - dk_exc < present_Nu && k - dk_exc >= 0) {
				// f0(k - dk_exc)

				if (is_even_or_zero(ell)) {
					uprime = g.u_e(k - dk_exc);
					totalICS = g.sigma_e(k - dk_exc, i_x);
				}
				else {
					uprime = g.u_o(k - dk_exc);
					totalICS = g.sigma_o(k - dk_exc, i_x);
				}
				u = uprime + (xsec->parent_eV_thresh() * mb::QE); // safer option if rotations exists

				double Phi = xsec->scattering()->scattering_integral(uprime / mb::QE, u / mb::QE, ell);

				// scattering calls for the energy of the leaving electron
				A.at((ell * present_Nu) + k, (ell * present_Nu) + k - dk_exc) +=
					+frac * uprime * Np * Phi * totalICS;

			}



			// scattering-out at u
			if (is_even_or_zero(ell)) {
				u = g.u_e(k);
				totalICS = g.sigma_e(k, i_x);
			}
			else {
				u = g.u_o(k);
				totalICS = g.sigma_o(k, i_x);
			}

			// electron scatters-out at u
			A.at((ell * present_Nu) + k, (ell * present_Nu) + k) +=
				-frac * u * Np * totalICS;

		}
	}
}
