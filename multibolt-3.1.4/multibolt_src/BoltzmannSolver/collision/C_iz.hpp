// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// ionization operator
// cooling effect - depending on energy-sharing, may insert large electron content to head
// hot electrons tend to be inserted midway and at low eV
void mb::BoltzmannSolver::C_iz(arma::sword ell, arma::SpMat<double>& A) {


		for (auto i_x : g.i_ionization) {

			auto xsec = g.xsec_ptr(i_x);
			auto spec = g.species_ptr(g.i_collision_species(i_x));
			double frac = g.i_frac(i_x);
			if (mb::doubles_are_same(frac, 0)) { continue; }

			mb::debug_statement("Applying C_ix[" + std::to_string(ell) + "] with process " + xsec->process() + " in species " + spec->name());

			// (Loureiro + Amorim notation for ionization)

			double u = 0; // energy of the scattered or secondary electron leaving the collision
			double uprime = 0; // pre-collision energy of the primary electron which creates the scattered and secondary (per-term)

			double totalICS = 0;

			


			for (arma::sword k = 0; k < present_Nu; ++k) {


				// steps in grid assosciated with ionization energy
				arma::sword dk_iz;
				if (is_even_or_zero(ell)) {
					dk_iz = arma::sword(round((xsec->eV_thresh()) * mb::QE / Du_at_u(g.u_e(k))));
				}
				else {
					dk_iz = arma::sword(round((xsec->eV_thresh()) * mb::QE / Du_at_u(g.u_o(k))));
				}

				// u idx for the primary scattered electron u
				double FRAC_PRIMARY = 1.0 / (1.0 - p.sharing);
				arma::sword dk_PRIMARY = arma::sword(round(FRAC_PRIMARY * k) + dk_iz);
				// in case of one-takes-all, all energy goes to the primary and the secondary generates at "zero" instead
				
				// u idx for the secondary electron uprime_secondary
				double FRAC_SECONDARY = 1.0 / p.sharing;
				arma::sword dk_SECONDARY = arma::sword(round((FRAC_SECONDARY) * k) + dk_iz);




				// --- Handle primary scattered electron
				
				// electron scatters-in at energy Zp*u + uk
				if(dk_PRIMARY < present_Nu && dk_PRIMARY >= 0) {
					if (is_even_or_zero(ell)) {
						uprime = g.u_e(dk_PRIMARY); // primaries with uprime create scattered with u
						totalICS = g.sigma_e(dk_PRIMARY, i_x); // iz->eval_at_k(g.EVEN, dk_PRIMARY);
					}
					else {
						uprime = g.u_o(dk_PRIMARY); // primaries with uprime create scattered with u
						totalICS = g.sigma_o(dk_PRIMARY, i_x);; //iz->eval_at_k(g.ODD, dk_PRIMARY);
					}

					// Per Loureiro, Amorim: primaries with uprime create scattered with u
					u = (uprime - xsec->eV_thresh() * mb::QE) * (1 - p.sharing);

					double Phi = xsec->scattering()->scattering_integral(uprime/mb::QE, u/mb::QE, ell);

					// f0(dk_iz_primary)
					A.at((present_Nu * ell) + k, (present_Nu * ell) + dk_PRIMARY) +=
						 frac * (FRAC_PRIMARY) * uprime * Np * Phi * totalICS;
					
				}
				


				
				// --- Handle secondary electron

				// limit case for one-takes-all
				if (doubles_are_same(p.sharing, 0)) {
					if (is_even_or_zero(ell)) {
						u = g.u_e(0); // secondary gets "nothing"
						uprime = g.u_e(k); // scattered electron essentially doesn't change energy from primary
						totalICS = g.sigma_e(k, i_x); //iz->eval_at_k(g.EVEN, k);
					}
					else {
						u = g.u_o(0); // secondary gets "nothing"
						uprime = g.u_o(k);  // scattered electron essentially doesn't change energy from primary
						totalICS = g.sigma_o(k, i_x);; //iz->eval_at_k(g.ODD, k);
					}

					double Phi = xsec->scattering()->scattering_integral(uprime / mb::QE, u / mb::QE, ell);

					// electron scatters-in at "0" energy 
					// f0(0) : insert at 0
					A.at((present_Nu * ell) + 0, (present_Nu * ell) + k) +=
						 frac * uprime * Np * Phi * totalICS;


				}
				else { // actual sharing
					if (dk_SECONDARY < present_Nu && dk_SECONDARY >= 0) {
						if (is_even_or_zero(ell)) {
							uprime = g.u_e(dk_SECONDARY); // electrons coming in at uprime create the secondary at u
							totalICS = g.sigma_e(dk_SECONDARY, i_x);
						}
						else {
							uprime = g.u_o(dk_SECONDARY); // electrons coming in at uprime create the secondary at u
							totalICS = g.sigma_o(dk_SECONDARY, i_x); 
						}

						// Per Loureiro, Amorim: primaries with uprime create secondary with u
						u = (uprime - xsec->eV_thresh() * mb::QE) * p.sharing;

						double Phi = xsec->scattering()->scattering_integral(uprime / mb::QE, u / mb::QE, ell);

						// electron scatters-in at energy Zs*u + uk 
						// f0(dk_iz_secondary)
						A.at((present_Nu * ell) + k, (present_Nu * ell) + dk_SECONDARY) +=
							 frac * (FRAC_SECONDARY)* uprime * Np * Phi * totalICS;
						

					}
				}
				



				// electron scatters-out at u 
				if (is_even_or_zero(ell)) {
					u = g.u_e(k); // primary electron energy which collides
					totalICS = g.sigma_e(k, i_x); 
				}
				else {
					u = g.u_o(k); // primary electron energy which collides
					totalICS = g.sigma_o(k, i_x);
				}

				A.at((present_Nu * ell) + k, (present_Nu * ell) + k) +=
					- frac * u * Np * totalICS;

			}
		}


}