// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"

// if not close to 1, sign of convergence issues
double mb::BoltzmannSolver::check_f0_normalization() {
	return arma::accu(trapz(g.eV_e, x_f(g.ell0_idx) % sqrt(g.eV_e)));
}




// Check if the grid maximum deserves to be adjusted
// to lower u_max, raise it, or leave it alone
// Make decision based on EEDF decay height
mb::RemapChoice mb::BoltzmannSolver::check_grid_max() {

	double fnorm = check_f0_normalization();

	if (fnorm < 0 || fnorm > 1.0 || arma::arma_isnan(fnorm) ) {
		mb::normal_statement("f0 fails normalization badly: cannot make a decision about grid-max.");
		return mb::RemapChoice::KeepGridMax;
	}

	if (fnorm < 0.5) {
		mb::normal_statement("f0 is 'realistic' but does not obey normalization: cannot make a decision about grid-max.");
		return mb::RemapChoice::KeepGridMax;
	}

	arma::uvec safe_ix = get_safe_eedf_ix();
	//x_f(g.ell0_idx(safe_ix)).print();
	double max_order = arma::max(arma::log10(x_f(g.ell0_idx(safe_ix))));
	double min_order = arma::min(arma::log10(x_f(g.ell0_idx(safe_ix))));
	double orderspan = max_order - min_order;


	if (orderspan > (p.remap_target_order_span + p.remap_allowance/2.)) { //  too many orders are covered

		if (present_eV_max <= 0.1) { // do not allow to plummet below a minimum roof
			mb::normal_statement("Remap scheme thinks grid-max should be lowered, but it is already very small. Grid will be kept.");
			return mb::RemapChoice::KeepGridMax;
		}
		return mb::RemapChoice::LowerGridMax;
	}
	else if (orderspan < p.remap_target_order_span - p.remap_allowance) { // grid roof is quite problematically low
		return mb::RemapChoice::RaiseGridMaxLots;
	}
	else if (orderspan < (p.remap_target_order_span - p.remap_allowance/2.)){
		return mb::RemapChoice::RaiseGridMaxLittle;
	}

	return mb::RemapChoice::KeepGridMax;
}

// lower roof of grid, ignore higher eV behavior to better resolve low-eV (or avoid a noise floor?)
void mb::BoltzmannSolver::lower_grid() {

	arma::uvec safe_ix = get_safe_eedf_ix();
	arma::colvec decay = log10(x_f(g.ell0_idx(safe_ix(0)))) - log10(x_f(g.ell0_idx(safe_ix)));

	arma::uvec pt = arma::find(decay > (p.remap_target_order_span ), 1, "first");

	arma::uword safe_pt = safe_ix(pt(0));
	present_eV_max = g.eV_e(safe_pt);

	mb::normal_statement("Grid roof was lowered to " + mb::mb_format(present_eV_max) + " eV.");

	// now reset grid
	set_grid();

}

// 2024-10-16 : Simplistic, may require extra iterations to find something useful
void mb::BoltzmannSolver::raise_grid_max_lots() {


	double new_eV_max = present_eV_max * 10.;
	present_eV_max = (present_eV_max + new_eV_max) / 2; // average the two results, better stability

	mb::normal_statement("Grid-max was raised to " + mb::mb_format(present_eV_max) + " eV.");

	// now reset grid 
	set_grid();
}

// deprecated
// 
// 2022-04-07 : guess where to raise the grid based on extrapolating out the tail, instead
// log/log linearity is a fairly safe assumption
// hopefully results in fewer guesses
void mb::BoltzmannSolver::raise_grid_max_extrap() {


	// The following doesn't work very well after all

	// find line: [log(y)] = m * [log(x)] + b	
	// which can extrapolate the tail (check near ~75% of roof)


	double avg_en = calculate_avg_en();
	//arma::colvec range_log10eV = { log10(avg_en) , log10(0.75 * present_eV_max) };
	arma::colvec range_log10eV = { log10(0.50 * present_eV_max) , log10(0.55 * present_eV_max) };

	arma::colvec range_log10eedf = arma::colvec(2, arma::fill::zeros);

	arma::interp1(arma::log10(g.eV_e), arma::log10(x_f(g.ell0_idx)), range_log10eV, range_log10eedf);
	double m = (range_log10eedf(1) - range_log10eedf(0)) / (range_log10eV(1) - range_log10eV(0)); // slope of log-lin
	double intercept = (m * (0.0 - range_log10eV(0))) + range_log10eedf(0);


	//taken from above, in lower_grid
	arma::uvec chopped = arma::find(g.ell0_idx >= arma::sword(present_Nu * 0.01) && g.ell0_idx < arma::sword(present_Nu * 0.90)); // avoid front head or back tail, avoids droop or odd bumps like in N2
	arma::uvec fbody_idx = arma::find_unique(x_f(g.ell0_idx));


	double existing_top = arma::max(arma::log10(x_f(arma::intersect(chopped, fbody_idx))));
	double target_bottom = existing_top - p.remap_target_order_span;

	double temp_present_eV_max = pow(10.0, (target_bottom - intercept) / m);

	if (arma::arma_isnan(temp_present_eV_max) || temp_present_eV_max < present_eV_max) {
		present_eV_max = 2. * present_eV_max;
		mb::normal_statement("Grid-max estimation returned nan, the EEDF may be poorly-behaved. Grid-max will be doubled instead.");
	}
	else {

		if (abs(temp_present_eV_max / present_eV_max) < 0.10) {
			raise_grid_max_little(); // do this instead rather than insist on barely nudging the grid
		}

		if (temp_present_eV_max / present_eV_max > 5) { // never let this try for more than x5 the current size
			temp_present_eV_max = present_eV_max * 5;
		}
		
			present_eV_max = temp_present_eV_max;
		
	}

	mb::normal_statement("Grid-max was raised to " + mb::mb_format(present_eV_max) + " eV.");

	// now reset grid 
	set_grid();
}

// Small nudge only
// 10/2024 - found out this was never being called... oops
void mb::BoltzmannSolver::raise_grid_max_little() {

	double new_eV_max = present_eV_max * 1.25;
	present_eV_max = (present_eV_max + new_eV_max) / 2; // average the two results, better stability

	mb::normal_statement("Grid-max was raised to " + mb::mb_format(present_eV_max) + " eV.");

	set_grid();
}




// todo: move this elsewhere
void mb::BoltzmannSolver::check_validity() {

	double fracsum = 0;

	// no gas should have a negative fraction
	for (auto& spec : lib.allspecies) {

		fracsum += spec->frac();

		if (spec->frac() < 0) {
			throw_err_statement("Cannot solve: Species must have positive fractional presence.");
		}

		for (auto& x : spec->allcollisions) {
			if (spec->frac() < 0) {
				throw_err_statement("Cannot solve: Xsecs must have positive scales.");
			}
		}

	}
	
	// limit energy sharing to meaningful values
	if (p.sharing > 0.5 || p.sharing < 0) {
		throw_err_statement("Cannot solve: Energy-sharing parameter (delta) must be in range [0, 0.5]");
	}

	// fractional portions of gases must add up to 1.0
	// edit: UNLESS YOU SPECIFICALLY ALLOW OTHERWISE
	if (abs(fracsum - 1.0) > 1e-5  && p.DONT_ENFORCE_SUM == false) {
		throw_err_statement("Cannot solve: Fractional sum must add to 1.0");
	}
	


	if (p.N_terms % 2 != 0) {
		throw_err_statement("Cannot solve: N_terms must be an even number.");
	}



	if (p.N_terms < 2) {
		throw_err_statement("Cannot solve: N_terms must be greater than or equal to 2.");
	}


	// Make sure you're not doubling up on ela or eff per-species
	for (auto& spec : lib.allspecies) {

		if (spec->n_eff() > 1) {
			throw_err_statement("Cannot solve: Assigning more than one effective Xsec to a species is disallowed.");
		}

		if (spec->n_eff() > 1 && spec->n_ela() > 0) {
			throw_err_statement("Cannot solve: Assigning an effective Xsec and an elastic Xsec to the same species simultaneously is disallowed.");
		}
	}

}


bool mb::BoltzmannSolver::check_is_solution_failing(int level) {

	// Disable SHY to see the results returned when the xsec is unrealistic
	// rather than assuming you're modeling a real gas

	if (p.SHY == true) {
	
		if (level == 0) {
			if (this->calculate_avg_en() < 0) {
				mb::error_statement("Solution failing: avg_en is unrealistic.");
				this->solution_succesful = false;
			}
			if (this->check_f0_normalization() < 0.75) {
				mb::error_statement("Solution failing: EEDF fails normalization.");
				this->solution_succesful = false;
			}

		}

		if (level == 1) {
			if (this->calculate_W_BULK() < 0) {
				mb::error_statement("Solution failing: W_BULK is unrealistic.");
				this->solution_succesful = false;
			}
			if (this->calculate_DFTN() < 0) {
				mb::error_statement("Solution failing: DFTN is unrealistic.");
				this->solution_succesful = false;
			}
			if (this->calculate_DFLN() < 0) {
				mb::error_statement("Solution failing: DFLN is unrealistic.");
				this->solution_succesful = false;
			}
		}

		if (level == 2) {
			if (this->calculate_DLN_BULK() < 0){
				mb::error_statement("Solution failing: DLN_BULK is unrealistic.");
				this->solution_succesful = false;
			}
			if (this->calculate_DTN_BULK() < 0 ){
				mb::error_statement("Solution failing: DTN_BULK is unrealistic.");
				this->solution_succesful = false;
			}
		}

	}

	return !(this->solution_succesful);


}