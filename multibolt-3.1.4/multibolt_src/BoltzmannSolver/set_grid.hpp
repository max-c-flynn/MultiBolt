// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"

// ocassionally the index value of the Du you need is a bit ambiguous
double  mb::BoltzmannSolver::Du_at_u(const double u) {
	double Du = 0;
	switch (g.grid_style) {
	case mb::GridStyle::LinearEnergy:
		Du = (present_eV_max / (present_Nu - 1)) * mb::QE;
		break;
	case mb::GridStyle::QuadraticEnergy:
		double present_v_max = sqrt(2.0 * present_eV_max * mb::QE / mb::ME);
		double Dv = (present_v_max / (present_Nu - 1));
		Du = sqrt(mb::ME * 2.0 * u) * Dv;
		break;
	}

	return Du;
};


// ocassionally the index value of the Du you need is a bit ambiguous
arma::colvec  mb::BoltzmannSolver::Du_at_u(const arma::colvec& u) {
	//arma::colvec Du(arma::size(g.idx), arma::fill::zeros);
	arma::colvec Du(arma::size(u), arma::fill::zeros); // oct 2024 bugfix: Du_at_u may now  be any size, not necessarily the whole grid.
	switch (g.grid_style) {
	case mb::GridStyle::LinearEnergy:
		Du.fill((present_eV_max / (present_Nu - 1)) * mb::QE);
		break;

	case mb::GridStyle::QuadraticEnergy:
		double present_v_max = sqrt(2.0 * present_eV_max * mb::QE / mb::ME);
		double Dv = (present_v_max / (present_Nu - 1));
		Du = sqrt(mb::ME * 2.0 * u) * Dv;
		break;
	}

	return Du;
};


// Set the grids on every cross section to that of u_e and u_o
void mb::BoltzmannSolver::set_grid() {


	this->g = mb::Grid(); // reset new grid

	this->g.grid_style = mb::GridStyle::LinearEnergy;
	//this->g.grid_style = mb::GridStyle::QuadraticEnergy; // for testing only, do not use in-general!

	double Du = 0, Dv = 0, present_v_max = 0;

	switch (g.grid_style) {
	case mb::GridStyle::LinearEnergy:

		Du = (present_eV_max / (present_Nu - 1)) * mb::QE;
		g.u_e = arma::linspace(0.5 * Du, Du * (double(present_Nu) - 0.5), present_Nu);
		g.u_o = arma::linspace(Du, present_Nu * Du, present_Nu);
		
		break;

	case mb::GridStyle::QuadraticEnergy:

		present_v_max = sqrt(2.0 * present_eV_max * mb::QE / mb::ME);
		Dv = (present_v_max / (present_Nu - 1));
		g.u_e = arma::linspace(0.5 * Dv, Dv * (double(present_Nu) - 0.5), present_Nu); // into grid of v first
		g.u_e = 0.5 * mb::ME * g.u_e % g.u_e; // translate to u

		g.u_o = arma::linspace(Dv, present_Nu * Dv, present_Nu);
		g.u_o = 0.5 * mb::ME * g.u_o % g.u_o;
		break;
	}



	g.eV_e = g.u_e / mb::QE;
	g.eV_o = g.u_o / mb::QE;

	g.ell0_idx = arma::regspace<arma::uvec>(0, 1, present_Nu - 1); // 0, 1, 2.....Nu-1 -> corresponds to ell=0 indices
	g.ell1_idx = g.ell0_idx + present_Nu; // Nu, Nu+1, Nu+2.....2*Nu-1 -> corresponds to ell=1 indices
	g.ell2_idx = g.ell1_idx + present_Nu; //corresponds to ell = 2 indices

	g.idx = arma::regspace<arma::uvec>(0, 1, present_Nu - 1);
	g.t_idx = arma::regspace<arma::uvec>(1, 1, present_Nu - 1); // truncated once from zero // useful for (k, k-1)
	g.idx_t = arma::regspace<arma::uvec>(0, 1, present_Nu - 2);// truncated once from u_max //  useful for (k+1, k)
	g.t_idx_t = arma::regspace<arma::uvec>(1, 1, present_Nu - 2);

	auto x = g.u_e(g.idx_t);

	mb::debug_statement("Applying even and odd grids (linear-spaced, per-energy).");

	//new: use grid allocation tied to BESolver instead
	g.N_species = lib.allspecies.size();
	g.N_collisions = 0;
	for (auto& spec : lib.allspecies) {
		g.N_collisions += spec->allcollisions.size();
	}

	g.sigma_e = arma::mat(present_Nu, g.N_collisions, arma::fill::zeros); 
	g.sigma_o = arma::mat(present_Nu, g.N_collisions, arma::fill::zeros);

	g.i_species = arma::regspace<arma::uvec>(0, 1, g.N_species-1);
	g.i_collision = arma::regspace<arma::uvec>(0, 1, g.N_collisions-1);
	g.i_codes = arma::Col<int>(g.N_collisions, arma::fill::value(-1));

	g.i_collision_species = arma::uvec(g.N_collisions, arma::fill::value(arma::datum::nan));

	g.i_frac = arma::colvec(g.N_collisions, arma::fill::zeros);


	g.xsec_ptr = arma::field< std::shared_ptr<lib::AbstractXsec>>(g.N_collisions, 1);
	g.species_ptr = arma::field< std::shared_ptr<lib::Species>>(g.N_species, 1);
	


	int N_elastic = 0, N_effective = 0, N_excitation = 0, N_ionization = 0, N_attachment = 0, N_superelastic = 0;

	int i_coll = 0;
	for (auto i : g.i_species) {

		N_elastic += lib.allspecies.at(i)->n_ela();
		N_effective += lib.allspecies.at(i)->n_eff();
		N_excitation += lib.allspecies.at(i)->n_exc();
		N_ionization += lib.allspecies.at(i)->n_iz();
		N_attachment += lib.allspecies.at(i)->n_att();
		N_superelastic += lib.allspecies.at(i)->n_sup();

		g.species_ptr(i) = lib.allspecies.at(i);

		for (int j = 0; j < (lib.allspecies.at(i)->allcollisions).size(); ++j) {

			g.xsec_ptr.at(i_coll) = (lib.allspecies.at(i)->allcollisions.at(j)); // another instance of pointer ref

			g.sigma_e.col(i_coll) = g.xsec_ptr.at(i_coll)->eval_at_eV(g.eV_e, p.interp_method);
			g.sigma_o.col(i_coll) = g.xsec_ptr.at(i_coll)->eval_at_eV(g.eV_o, p.interp_method);

			g.i_codes(i_coll) = g.xsec_ptr.at(i_coll)->code();

			g.i_frac(i_coll) = lib.allspecies.at(i)->frac(); // todo: change to divorce frac from lib

			g.i_collision_species(i_coll) = i;

			i_coll++;
		}
	}

	g.i_elastic = arma::find(g.i_codes == lib::CollisionCode::elastic);
	g.i_effective = arma::find(g.i_codes == lib::CollisionCode::effective);
	g.i_excitation = arma::find(g.i_codes == lib::CollisionCode::excitation);
	g.i_ionization = arma::find(g.i_codes == lib::CollisionCode::ionization);
	g.i_superelastic = arma::find(g.i_codes == lib::CollisionCode::superelastic);
	g.i_attachment = arma::find(g.i_codes == lib::CollisionCode::attachment);



	arma::uvec ix_nonfinite_even = arma::find_nonfinite(g.sigma_e);
	arma::uvec ix_nonfinite_odd = arma::find_nonfinite(g.sigma_o);
	bool cannot_continue = false;
	for (auto i : g.i_collision) {
		if (arma::any(arma::find_nonfinite(g.sigma_e.col(i))) || arma::any(arma::find_nonfinite(g.sigma_o.col(i)))) {
			mb::normal_statement("The following xsec is gridded with nonfinite values:");
			g.xsec_ptr.at(i)->print();
			cannot_continue = true;
		}
		if (arma::any(g.sigma_e.col(i) < 0) || arma::any(g.sigma_o.col(i) < 0)) {
			mb::normal_statement("The following xsec is gridded with negative values:");
			g.xsec_ptr.at(i)->print();
			cannot_continue = true;
		}
	}
	if (cannot_continue){
		mb::throw_err_statement("Solution cannot continue: Gridding procedure failed. Check above for Xsec warning statements.");
	}
	


	// Based on this grid, figure out if iterations are actually needed
	// in order to not waste time iterating

	double accum_iz = 0;
	double accum_att = 0;

	for (auto i : g.i_collision) {
		auto this_code = g.xsec_ptr.at(i)->code();
		if (this_code == lib::CollisionCode::ionization) {
			accum_iz += arma::accu(g.sigma_e.col(i));
		}
		else if (this_code == lib::CollisionCode::attachment) {
			accum_att += arma::accu(g.sigma_e.col(i));
		}
	}



	if ((doubles_are_same(accum_att, 0) && doubles_are_same(accum_iz, 0))) {
		NO_ITERATION = true;
		mb::debug_statement("Detected that grid is all-conservative processes: all calculations will be single-iteration.");
	}
	else {
		NO_ITERATION = false; // ***
	}



	mb::debug_statement("Begin loading collision matrix.");
	
	// pre-load the scattering matrix.

	this->A_scattering = arma::SpMat<double>(present_Nu * p.N_terms, present_Nu * p.N_terms); // set to zeros
	for (arma::sword ell = 0; ell < p.N_terms; ++ell) {
		full_collision_operator(ell, this->A_scattering);
	}

	mb::debug_statement("Finishing loading collision matrix.");
};


