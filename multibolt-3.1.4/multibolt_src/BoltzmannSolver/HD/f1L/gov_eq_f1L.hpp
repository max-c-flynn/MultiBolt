// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"



void mb::BoltzmannSolver::gov_eq_f1L(arma::sword ell, arma::SpMat<double>& A, arma::Col<double>& b) {

	// heirarchy convenience
	gov_eq_f0_HD(ell, A);


	double u = 0;
	
	double ell_plus_one = ell + 1.0;
	double two_ell_plus_three = 2.0 * ell + 3.0;
	double two_ell_minus_one = 2.0 * ell - 1.0;



	// todo: figure out how to better abstract this

	for (arma::sword k = 0; k < present_Nu; ++k) {

		arma::sword kcol;
		arma::sword kcol2;
		if (ell % 2 == 0 || ell == 0) {
			u = g.u_e.at(k);
			kcol = k - 1;
			kcol2 = k + 1;
		}
		else {
			u = g.u_o.at(k);
			kcol = k + 1;
			kcol2 = k - 1;
		}


		double f0 = x_f(k + ell * present_Nu);
		double f0_plus = 0;
		double f0_minus = 0;

		// if kcol is a valid index, use the midpoint to find x_f
		// note: this is just using interpolation to move f+ and f- to the correct even/odd grid.
		// but directly assumes a uniform u grid (ie interpolation == find midpoint)
		// this MUST change to general interpolation to accomodate other grid types.
		// changes must also propogate to f1T, f2L, f2T
		if (kcol >= 0 && kcol < present_Nu) {

			if (ell < p.N_terms - 1) { f0_plus = (x_f(k + (ell + 1) * present_Nu) + x_f(kcol + (ell + 1) * present_Nu)) / 2.0;  }
			else { f0_plus = 0; }

			if (ell > 0) { f0_minus = (x_f(k + (ell - 1) * present_Nu) + x_f(kcol + (ell - 1) * present_Nu)) / 2.0; }
			else { f0_minus = 0; } 
			
		}
		else {

			if (ell < p.N_terms - 1) {
				f0_plus = (x_f(k + (ell + 1) * present_Nu) )/2.0;
			}
			else { f0_plus = 0; }

			if (ell > 0) {
				f0_minus = x_f(k + (ell - 1) * present_Nu)/2.0;
			}
			else { f0_minus = 0; }
		}


		b(k + ell * present_Nu) += 
			Np * u * (ell / two_ell_minus_one * f0_minus + ell_plus_one / two_ell_plus_three * f0_plus)
			- Np * sqrt(u * mb::ME / 2.0) * omega1 * f0;


	}

}