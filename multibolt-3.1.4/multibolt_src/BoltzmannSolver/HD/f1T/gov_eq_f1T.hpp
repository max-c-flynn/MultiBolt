// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"



// The following are simimlar to the case of f0, but use different factors of ell!

// governing equation for terms of (ell + 1) (f1T)
double eq_f1T_ell_plus_one(double u, arma::sword ell, double E0, double Np, double Du, bool is_forward) {
	int SIGN = is_forward ? +1 : -1;
	return (SIGN * mb::QE * E0 * (ell + 2.0) / (2.0 * ell + 3.0) * u / Du
		+ mb::QE * E0 * (ell + 2.0) / (2.0 * ell + 3.0) * (ell + 2.0) / 2.0 / 2.0);

};

// governing equation for terms of (ell - 1) (f1T)
double eq_f1T_ell_minus_one(double u, arma::sword ell, double E0, double Np, double Du, bool is_forward) {
	int SIGN = is_forward ? +1 : -1;
	return (SIGN * mb::QE * E0 * (ell - 1.0) / (2.0 * ell - 1.0) * u / Du
		- mb::QE * E0 * (ell - 1.0) / (2.0 * ell - 1.0) * (ell - 1.0) / 2.0 / 2.0);

};



void mb::BoltzmannSolver::gov_eq_f1T(arma::sword ell, arma::SpMat<double>& A, arma::Col<double>& b) {


	
	double u = 0;
	double Du = 0;

	bool forward = true;
	bool backward = false;

	double two_ell_plus_three = 2.0 * ell + 3.0;
	double two_ell_minus_one = 2.0 * ell - 1.0;
	


	for (arma::sword k = 0; k < present_Nu; ++k) {
		// set index for first derivative effect
		arma::sword kcol = k; // k-prime
		arma::sword kcol2 = k; // k-prime

		if (ell % 2 == 0 || ell == 0) { // if is even
			kcol = k - 1;
			kcol2 = k + 1;

			u = g.u_e.at(k);
			Du = Du_at_u(u);

			forward = true;
			backward = false;
		}
		else { // if is odd
			kcol = k + 1;
			kcol2 = k - 1;
			u = g.u_o.at(k);
			Du = Du_at_u(u);

			// flip derivative direction
			forward = false;
			backward = true;
		}

		// fill in a manner similar to that for f0_HD
		// Apply for f_ell
		A.at(k + (ell * present_Nu), k + (ell * present_Nu)) += eq_f0_HD_ell(u, omega0, Np); // okay to reuse expression


		// Apply for ell+1
		if (ell + 1 < p.N_terms) {	// if ell+1 exists
			// f_(l + 1)(k)
			A.at(k + (ell * present_Nu), k + (ell + 1) * present_Nu) += eq_f1T_ell_plus_one(u, ell, E0, Np, Du, forward);

			if (kcol >= 0 && kcol < present_Nu) { // if kcol index exists
				// f_(l + 1)(k - 1)
				A.at(k + (ell * present_Nu), (kcol)+(ell + 1) * present_Nu) += eq_f1T_ell_plus_one(u, ell, E0, Np, Du, backward);
			}
		}

		// Apply for ell - 1
		if (ell - 1 >= 0) {	// if ell-1 exists
			// f_(l - 1)(k)
			A.at(k + (ell * present_Nu), k + (ell - 1) * present_Nu) += eq_f1T_ell_minus_one(u, ell, E0, Np, Du, forward);

			if (kcol >= 0 && kcol < present_Nu) { // if kcol index exists 
				// f_(l - 1)(k + 1)
				A.at(k + (ell * present_Nu), (kcol)+(ell - 1) * present_Nu) += eq_f1T_ell_minus_one(u, ell, E0, Np, Du, backward);
			}
		}



		// todo: figure out how to better abstract this


		double f0_minus = 0;
		double f0_plus = 0;

		

		// if kcol is a valid index, use the midpoint to find x_f
		if (kcol >= 0 && kcol < present_Nu) {
			f0_minus = (x_f.at(k + (ell - 1) * present_Nu) + x_f.at(kcol + (ell - 1) * present_Nu)) / 2.0;


			if (ell < p.N_terms-1) { f0_plus = (x_f.at(k + (ell + 1) * present_Nu) + x_f.at(kcol + (ell + 1) * present_Nu)) / 2.0;}
			else { f0_plus = 0; }

		}
		else {
			f0_minus = (x_f.at(k + (ell - 1) * present_Nu) )/2.0;


			if (ell < p.N_terms-1) {
				f0_plus = (x_f.at(k + (ell + 1) * present_Nu)) / 2.0;
			}
			else { f0_plus = 0; }
		}


		b.at(k + ell * present_Nu) +=
			Np * u * (f0_minus / two_ell_minus_one - f0_plus / two_ell_plus_three);
		
	
	}

}
