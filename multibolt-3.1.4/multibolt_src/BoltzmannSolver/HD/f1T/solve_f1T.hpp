// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// solve governing equation for 1T gradient
void mb::BoltzmannSolver::solve_f1T() {

	mb::debug_statement("Begin solving governing equation for f1T.");

	arma::sword N = present_Nu * p.N_terms;

	arma::SpMat<double> A(N, N);
	// special consideration for f1T: only the ell != 0 submats matter
	// resizing will occur later, full size is kept to match the collision operator functions

	arma::colvec b(N, arma::fill::zeros);

	x_f1T = arma::colvec(N, arma::fill::zeros);

	// Note: f1T has a direct solution and needs only one iteration
	// and is unfilled for ell == 0!
	for (arma::sword ell = 1; ell < p.N_terms; ++ell) {
		gov_eq_f1T(ell, A, b);
	}
	A = A - A_scattering;


	//arma::uvec idx = arma::regspace<arma::uvec>((present_Nu + 1), N, 1);



	double norm = arma::max(arma::max(A));
	A = A / norm;
	b = b / norm;

	// no normalization condition, ell=0 does not exist

	// - scheme insensitive to BC
	//// Boundary conditions 
	//for (arma::sword ell = 0; ell < p.N_terms; ++ell) {
	//	boundary_conditions(ell, A, b);
	//}

	// resize for actual solution
	A = A.tail_rows(N - present_Nu);
	A = A.tail_cols(N - present_Nu);
	b = b.tail_rows(N - present_Nu);



	arma::colvec temp_x_f1T;
	
	bool solve_success = arma::spsolve(temp_x_f1T, A, b, "superlu", multibolt_superlu_opts()); // solve
	if (!solve_success) {
		mb::error_statement("Solution failing: spsolve unsuccesful.");
		this->solution_succesful = false;
		temp_x_f1T.set_size(N);
		temp_x_f1T.fill(arma::datum::nan);
		return;
	}

	x_f1T.subvec(present_Nu, N - 1) = temp_x_f1T; // move back into correct size
	x_f1T.subvec(0, present_Nu - 1).zeros();

	mb::debug_statement("f_1T solved in single iteration.");
	mb::display_iteration_banner("f1T", 1, "DFTN [m^-1 s^-1]", calculate_DFTN(), 0);

	mb::debug_statement("Exit solving governing equation for f1T.");

}