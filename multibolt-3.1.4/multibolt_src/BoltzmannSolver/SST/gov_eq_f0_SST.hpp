// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



void mb::BoltzmannSolver::gov_eq_f0_SST(arma::sword ell, arma::SpMat<double>& A) {

	// lambda: governing equation for terms of (ell + 1)
	auto eq_f0_SST_ell_plus_one = [](double u, arma::sword ell, double alpha_eff_N, double E0, double Np, double Du, bool is_forward) {
		int SIGN = is_forward ? +1 : -1;
		return (SIGN * mb::QE * E0 * (ell + 1.0) / (2.0 * ell + 3.0) * u / Du
			+ mb::QE * E0 * (ell + 1.0) / (2.0 * ell + 3.0) * (ell + 2.0) / 2.0 / 2.0
			+ (ell + 1.0) / (2.0 * ell + 3.0) * u / 2.0 * alpha_eff_N * Np);
	};

	// lambda: governing equation for terms of (ell - 1)
	auto eq_f0_SST_ell_minus_one = [](double u, arma::sword ell, double alpha_eff_N, double E0, double Np, double Du, bool is_forward) {
		int SIGN = is_forward ? +1 : -1;
		return (SIGN * mb::QE * E0 * ell / (2.0 * ell - 1.0) * u / Du
			- mb::QE * E0 * ell / (2.0 * ell - 1.0) * (ell - 1.0) / 2.0 / 2.0
			+ ell / (2.0 * ell - 1.0) * u / 2.0 * alpha_eff_N * Np);
	};


	// in this context, is essentially everything from the original solve_A that excludes the collision operator

	arma::colvec u;
	bool forward = true;
	bool backward = false;

	if (ell % 2 == 0 || ell == 0) {
		u = g.u_e;
	}
	else {
		u = g.u_o;

		// flip derivative direction
		forward = !(forward);
		backward = !(backward);
	}

	for (arma::sword k = 0; k < present_Nu; ++k) {

		double Du = 0; 

		// set index for first derivative effect
		arma::sword kcol = k;
		if (ell % 2 == 0 || ell == 0) { // if is even
			kcol = k - 1;
			Du = Du_at_u(u(k));
		}
		else { // if is odd
			kcol = k + 1;
			Du = Du_at_u(u(k));
		}

		// Apply for f_ell - does not exist for SST!

		// Apply for ell+1
		if (ell + 1 < p.N_terms) {	// if ell+1 exists
			// f_(l + 1)(k)
			A.at(k + (ell * present_Nu), k + (ell + 1) * present_Nu) += eq_f0_SST_ell_plus_one(u.at(k), ell, alpha_eff_N, E0, Np, Du, forward);

			if (kcol >= 0 && kcol < present_Nu) { // if kcol index exists
				// f_(l + 1)(k - 1)
				A.at(k + (ell * present_Nu), (kcol)+(ell + 1) * present_Nu) += eq_f0_SST_ell_plus_one(u.at(k), ell, alpha_eff_N, E0, Np, Du, backward);
			}
		}

		// Apply for ell - 1
		if (ell - 1 >= 0) {	// if ell-1 exists
			// f_(l - 1)(k)
			A.at(k + (ell * present_Nu), k + (ell - 1) * present_Nu) += eq_f0_SST_ell_minus_one(u.at(k), ell, alpha_eff_N, E0, Np, Du, forward);

			// f_(l - 1)(k + 1)
			if (kcol >= 0 && kcol < present_Nu) { // if kcol index exists 
				A.at(k + (ell * present_Nu), (kcol)+(ell - 1) * present_Nu) += eq_f0_SST_ell_minus_one(u.at(k), ell, alpha_eff_N, E0, Np, Du, backward);
			}
		}
	}


}


